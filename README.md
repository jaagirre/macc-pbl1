# Ejemplos Terraform básicos para MACC **PBL1**

En este repositorio se os ofrece una base de terraform como ayuda y base para el PBL. En este repo se os ofrecen 3 ejemplos diferentes:****

1. **infrastructure/global** : Creacion de una VPC con dos AZ y dos subredes publicas y dos subredes privadas (con NAT, IGW y SG)****
2. **infrastructure/app_bastion**:Despliegue en el VPC anterior de una instancia EC2 a utilizar como Bastion y que tambien despliega dos contenedores (Api Rest y BBDD)
3. **infrastructure/app_bastion_alb_rds_privte_ec2**: Se despliega el bastion , un RDS, un ELB, un target group en la zona privada y se agrega una instancia Ec2 con el API Rest que via AWS secrets obtiene las credenciales de acceso a la BBDD
4. **infrastructure/app_ecs**: Despliegue del mismo servicio pero en lugar de en EC2 via ECS

A conitnuacion os resumimos sucintamente los conceptos de terraform utiles que podeis encontrar en cada ejemplo.

#### VPC: **infrastructure/global**

En este carpeta teneis el ejemplo de un proyecto simple de Terraform. Simplemente creamos una VPC via el proveedor AWS de terraform.Esta parte la teneis con explicaciones detalladas en el siguiente apartado de este README. Simplemente creamos recursos, y parametrizamos minimamente via variables de terraform (variables.tf) los valores de las IPs de las subredes. Tambien utilizamos los outputs para ofrecer al finalziar los ID de los recursos del VPC creado. Tambien se utiliza un backend para guardar el estado y datos de los elementos creados,de forma que desde otros modulos  y proyectos terraform podamos hacer referencia a los recursos creados, esto lo hacemos con el denominado **backend local de terraform**, una especie de cache donde se guarda información que se puede compartir entre modulos Terraform. Resumiendo en este proyecto teneis el main.tf, las variables, los outputs y la creacion de la red y los security groups.

Ficheros utilizados para la creación de VPCs
```bash
main.tf  network.tf  outputs.tf  security_groups.tf  variables.tf

```

En el apartado [Enlace a la sección "Introducion a terraform: Creando la VPC paso a paso"](#introducion-a-terraform--creando-la-vpc-paso-a-paso) teneis explicado como crear paso por paso la VPC desde la instalacion de la herramienta terraform, para asi comenzar con terraform. En el siguiente link teneis la misma informacion mas el tema de ecs [Enlace al archivo ejemplo_terraform_ecs.md](doc/ejemplo_terraform_ecs.md)

Para lanzar la creación de la VPC

```bash
$cd infrastructure/global/
$terraform init
$terraform validate
  Success! The configuration is valid.
$terraform plan -out mi_plan
  Plan: 19 to add, 0 to change, 0 to destroy.

  Changes to Outputs:
    + apps_security_group_id          = (known after apply)
    + bastion_security_group_id       = (known after apply)
    + load_balancer_security_group_id = (known after apply)
    + private_subnet_1_id             = (known after apply)
    + private_subnet_2_id             = (known after apply)
    + public_subnet_1_id              = (known after apply)
    + public_subnet_2_id              = (known after apply)
    + vpc_id                          = (known after apply)

  ─────────────────────────────────────────────────────────────

  Saved the plan to: mi_plan

  To perform exactly these actions, run the following command to apply:
      terraform apply "mi_plan"

$ terraform apply mi_plan
aws_vpc.pbl-vpc: Creating...
Apply complete! Resources: 19 added, 0 changed, 0 destroyed.

Outputs:

apps_security_group_id = "sg-07d0b1e00d030e547"
bastion_security_group_id = "sg-01e305a1eabcf6feb"
load_balancer_security_group_id = "sg-0ec5b21dae893e6e3"
private_subnet_1_id = "subnet-0bb8c1de0278e2a1a"
private_subnet_2_id = "subnet-00936f721a921546f"
public_subnet_1_id = "subnet-0215d70e5ea783270"
public_subnet_2_id = "subnet-022bc05488da1f8a7"
vpc_id = "vpc-03fdf20f338681278"

```



#### infrastructure/app_bastion

En este ejemplo  la idea es utilizando un AMI crear un Bastion en la subred publica , copiarle a este un fichero .pem que tenemos en local, el cual se utilizara para conectar a las demas máquinas EC2. Tambien crearemos un user_data que lanzara un docker-compose con un API REST implementado en FAST_API junto con una BBDD Postgres.

Todo lo relacionado con la instancia de Bastion se definira en la carpeta **module_app**, con la posibilidad de utilizarlo como modulo (sin main.tf), de forma que podremos crear tantos modulos como queramos de este elemento pero con valores de las variables diferentes.En este ejemplo solo crearemos un modulo llamado **service** y a este le pasaremos informacion del VPC utilizando el backend de terraform , tambien le pasaremos valores concretos de manera que instanciaremos con unos valores concretos. Al user data del Bastion le pasaremos variables de entorno, algo muy util para vuestro proyecto. Y el user data lanza un docker-compose , algo tambien util para el PBL.

Aqui teneis el main.tf que crea el modulo **service** con valores concretos y valores del VPC via datos basado en el backend de terraform. Para aprender mas tendreis que navegar en el código, es bastante autoexplicativo;-) Decir que al crear la instancia tambien utilizamos ciertos recursos para copiar y ejecutar unos comandos remotamente en la instancia. En este ejemplo el **LabRole** por ejemplo no esta parametrizado, y lo normal seria parametrizar los usuarios IAM y los roles. Algunas cosas en este ejemplo estan simplificadas para facilitar el aprendizaje incremental.

En la carpeta raiz de **app_bastion**  tenemos el fichero main.tf, la carpeta **key/** donde estará el .pem, **variables.tf** donde definimos las variables que queremso utilizar, **outputs.tf**  donde definimos los valores de salida que queremos visualizar y en **modulo_app** definimos las instancias Ec2. 

```bash
$ls app_bastion/
  key  main.tf  module_app  outputs.tf  variables.tf
$ls app_bastion/module_app
ls app_bastion/module_app/
  instance.tf  logs.tf  outputs.tf  templates.tf  user_data  variables.tf

```
A continuación tenemos el contenido de main.tf.El **main.tf** crea una instancia del modulo **app_bastion/module_app**. el modulo es todo el conetndio de la carpeta , y main lo que hace crear una instancia del modulo dando valor a las variables del modulo **app_bastion/module_app/variables.tf**.

En el **main.tf**  se define un elemento  **data** de terraform   del  tipo **terraform_remote_state** que llamamos **vpc**,  donde tenemos los datos de la ejecución anterior de Terraform. Mediante este data podemos asignar valores a ciertas variables en esta ejecución de terraform, por ejemplo podemos decir que para este terraform el vpc_id sera el id del vpc que hemos creado antes y que estaba como output de la ejeucion terraform anterior.


```tf
terraform{
  backend "local"{}
}

data "terraform_remote_state" "vpc" {
  backend = "local"

  config = {
    path = "../global/terraform.tfstate"
  }
}

provider "aws" {
  region = var.region
}


module "service" {
  source = "./module_app"


  vpc_id = data.terraform_remote_state.vpc.outputs.vpc_id // "vpc-0e2859b192444dd56"
  apps_security_group_id = data.terraform_remote_state.vpc.outputs.bastion_security_group_id // "sg-05fc775dfed946b74"
  public_subnet_1_id = data.terraform_remote_state.vpc.outputs.public_subnet_1_id// "subnet-0a413a2243f0cfeb8"
  public_subnet_2_id = data.terraform_remote_state.vpc.outputs.public_subnet_2_id// "subnet-0c542c4d102e094ff"
  private_subnet_1_id = data.terraform_remote_state.vpc.outputs.private_subnet_1_id// "subnet-08033e39d51d783a2"
  private_subnet_2_id = data.terraform_remote_state.vpc.outputs.private_subnet_2_id//"subnet-04456c48b8e908718"

  bastion-instance-type = "t3.medium"
  log_retention_in_days = 30
  region = var.region
  app_environment = "development"
  ami="ami-05f8f5a7f96c4acca"
}
```

Mediante  **data.terraform_remote_state.vpc** podemos asignar diferentes valores a ciertas variables en esta ejecucicón de terraform, por ejemplo podemos decir que para este terraform el vpc_id sera el que hemos creado antes y luego al crear la instancia ec2 ubicarlo en esa vpc.

En el main.tf l edamos un valor a la variable public_subnet_1_id.
```tf
public_subnet_1_id = data.terraform_remote_state.vpc.outputs.public_subnet_1_id
```

Y en instance.tf podemos decir que la instancia se ubicara en la subred via la variable.
```tf
 subnet_id = var.public_subnet_1_id 
```

Y la variable del modulo se ha definido en **module_app/variables.tf**. Y en el main.tf se le ha dado un valor que no es el de por defecto;-)

```tf
variable "public_subnet_1_id" {
  description = "Id of first public subnet"
  type = string
}
```

Por otra parte es interesante ver como se ha creado el user_data. Para ello tenemos el .sh enla carpeta **user_data** y en el fichero **module_app/tenplates.tf** creamos un **data** terraform del tipo **template_file** que llamaremos **bastion**. Luego el **template_data** bastion se lo podemos asignar a un recurso Terraform en este caso a una instancia Ec2;-)

```tf
# User data templates
data "template_file" "bastion" {
  template = file("${path.module}/user_data/bastion.sh")

  vars = {
    bastion-hostname      = "bastion"
    # service-port          = 443
    # kibana-host           = aws_instance.microservices-observability.private_ip 
    aws-region            = var.region
  }
}

```

Y en la instancia **module_app/instance.tf**,

```tf
resource "aws_instance" "bastion" {
  ami                         = var.ami
  instance_type               = var.bastion-instance-type
  key_name                    = var.key-name
  security_groups             = [var.apps_security_group_id]
  subnet_id                   = var.public_subnet_1_id 
  associate_public_ip_address = true
  user_data                   = data.template_file.bastion.rendered


```

Una vez entenddo los conceptos ejecutamos este terraform.recordar que se tiene qu tener creada la VPC anterior.

```bash
$ cd infrastructure/app_bastion
$ terraform validate
$ terraform plan -out mi_plan_bastion
  Plan: 3 to add, 0 to change, 0 to destroy.

  Changes to Outputs:
    + bastion = (known after apply)
──────────────────────────────────────

Saved the plan to: mi_plan_bastion

$ terraform apply mi_plan_bastion
  Apply complete! Resources: 3 added, 0 changed, 0 destroyed.

  Outputs:

  bastion = "ec2-98-81-4-132.compute-1.amazonaws.com"

$ curl   ec2-98-81-4-132.compute-1.amazonaws.com/health-check/
{"message":"OKv9","environment":"local","debug":true,"test":true}

```

El resultado de la ejecucion de esta parte resultaria en una maquina Bastion con ip publica que ofrece en el puerto 80 un API REST. A continuación tenemos como se crea el Bastion. Para ello se tiene que tener creada la VPC  del paso anterior.



![api rest en bastion](doc/images/terraform_bastion.png)



#### infrastructure/app_bastion_alb_rds_privte_ec2

En este ejmplo ademas de crear el Bastion de antes se creara una instancia privada de un EC2 que en este caso expondra el API REST en el puerto 8000 via un Load Balancer , pr lo que estara en un target grpup. En este caso la base dedatos sera un RDS, por lo que el terraform tendra que esperar a la creacion de la BBDD para crear la instancia , en este caso llamda **Posts**. Esto lo gestiona terraform creando un arbol de dependencias anazliando las asiganciones de valores y variables;-)

Al crear el rds tambien se crean los secretos (es importante decir que el codigo de la aplicaion ejemplo , se configura para que dependiendo del entorno que le indicamos cogera la informacion del endpoint de diferentes lugares, en este caso le indicaremos que la APP_ENVIRONMENT=development, lo cual le forza al codigo a buscar el endpoint via el secreto AWS "db-connection-string-{self.APP_ENVIRONMENT}. Esto lo podeis ver en el codigo de la aplicacion **src/services/config.py**, pero esto es solo un ejemplo, vosotros en vuestro codigo lo hareis como veais ;-). Despues Terraform creara la instancia que depende de este secreto (lo decide el mediante un arbol de ejecucion que crea terraform al leer todo los ficheros y mirar sus dependencias).

Es importante decir , que al secreto tiene la politica por defecto,
es decir , que al borrar el secretoi esta se mantiene durante una ventana de 7 dias. Debido a esto si hacemos **terraform destroy** y luego **apply**, os dira que no puede crear el secreto porque ya existe. Para borrar hay que utilizar una llamada concreta
al API de aws. Lo teneis a continuación.

```bash
aws secretsmanager delete-secret --secret-id db-connection-string-development  --force-delete-without-recovery --region us-east-1
terraform refresh
```

Ahora crearemos otra instancia en la subred privada. Esta instancia solo ejecutara el docker del endpoint (por lo tanto no ejecutara el servicio via docker-compose, sin el db), y le pasaremos la variable de enotrno APP_ENVIROMENT=development, y asi se configurara contra el RDS;-)

Salida de creacion bastion+rds+elb+ec2posts

```bash
Apply complete! Resources: 14 added, 0 changed, 0 destroyed.

Outputs:

bastion = "ec2-3-89-73-224.compute-1.amazonaws.com"
load_balancer_dns = "development-alb-815286972.us-east-1.elb.amazonaws.com"

```

![api rest via elb](doc/images/terraform_alb_rds.png)

Para conocer los detalles teneis que analziar el codigo. En este caso hay un segundo user_data , al que la pasamos variables de entorno que contiene informacion de lso elementso creados por terraform. Para forzar que la instancia sea lo ultimo en crear hemos asignado elvalor del secreto a la instancia POST , auqnue luego no hace falta ya qu elo busca programticamente via el valor de APP_ENVIRONMENT, pero hemso hecho esto para forzar el orde de creacion , y asi poder utilizar en unso recuros sla informacin (IP, endpoint ) de otros recursos.

```terraform
# templates.tf
# User data templates
data "template_file" "posts" {
  template = file("${path.module}/user_data/posts.sh")

  # Estas son las variables de entorno que la pasamos a la Instancia
  vars = {
    bastion-hostname      = "posts"
    aws-region            = var.region
    app-environment = var.app_environment
    rds_secrets_value = aws_secretsmanager_secret_version.db_connection_string.secret_string # ESTA ES LA LINEA QUE FORZARA EL ORDEN
  }
}

```

Aqui por ejemplo vemos que el LabRole se lo hemos asignado sin parametrizar, por lo tanto solo puede ser ejecutado con ese Role :-(.

```tf
resource "aws_instance" "posts" {
  ami                         = var.ami
  instance_type               = var.bastion-instance-type
  key_name                    = var.key-name
  security_groups             = [var.apps_security_group_id]
  subnet_id                   = var.private_subnet_1_id
  associate_public_ip_address = false
  user_data                   = data.template_file.posts.rendered
  iam_instance_profile        = "LabInstanceProfile"
  lifecycle {
    create_before_destroy = true
  }
  tags = {
    "Name" = "macc-posts"
  }

}

```

En el siguiente fichero **md** teneis la creacion del mismo servicio via ECS , explicando en detalle todos los tf de terraform.

## Introducion a terraform : Creando la VPC paso a paso

En este caso utilizaremos Terraform para gestion de la infrastructura. Primero los instalaremos.

```bash
wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
sudo apt update && sudo apt install terraform
terraform -v
  Terraform v1.3.7
  on linux_amd64
```
Tambien teneis Terraform para windows. Para instalarlo ir a su web y descargar el binario e instalarlo. Aseguraros de que meteis en el Path del sistema el binario.

Una vez instalado creamos la carpeta **infrastructure**, en la cual alojaremos el código que gestionara la infrastructura.
Dentro de esta crearemos la carpeta **global**, y sobre esta **vpc**.Y en la carpeta **vpc** creamos el fichero main.tf.
De momento solo indicamos en este fichero que el backend que utilizaremos para guardar el estado de la infrastructura
será local. Mas adelante, si incluimos la infraestructura en el pipeline de un repositorio git (por ejemplo gitlab), se podría gestionar el backend desde gitlab via http.

```terraform
terraform{
  backend "local"{}
}
```

Y ahora inicializamos Terraform desde la carpeta **vpc**, haciendo simplemente init.
Y vemos como esto crea la carpeta **.terraform** donde el backend local guardara el estado. Estos datos sobre los recursos creados se  podran utilizar en otros proyectos/modulos/ejecuciones de Terraform

```bash
cd infrastructure/global
terraform init
  Initializing the backend...
ls -la
  -rw-rw-r-- 1 main.tf
  drwxr-xr-x 2 .terraform
```

Y ahora, aunque todavia no hayamos definido recursos, comenzamos con el flujo clasico de terraform: validate, plan y apply.

```bash
terraform validate
  Success! The configuration is valid.
terraform plan
  No changes. Your infrastructure matches the configuration.
terraform apply
  Apply complete! Resources: 0 added, 0 changed, 0 destroyed.
```

terraform apply -target=aws_alb_listener.ecs-alb-http-listener
El estado no lo subiremos al repo por lo que añadimos al .gitignore esta carpeta y los ficheros de estado.

```bash
# Local .terraform directories
**/.terraform/*

# .tfstate files
*.tfstate
*.tfstate.*
```

Ya estamos preparados para comenzar a crear nuestra infraestructura.
En este caso empezaremos creando primero la vpc sobre la que haremos el despliegue de ECS y RDS.

- VPC
La VPC que crearemos tendra 2 subredes publicas y 2 subredes privadas, desplegadas en 2 zonas de disponibilidad.
Las aplicaciones , en este caso la aplicación dockerizada la ubicaremos en las subredes privadas, al igual que
la base de datos.Para dar salida a internet a los elementos ubicados en las subredes privadas tendremos un servicio NAT para dar salida a
internet con ip publica a los elementos con ip privadas. Tambien tendremos un Internet gateway. Ademas de estos elementos crearemos las tablas de rutado correspondientes, security groups y acls.

En **main.tf** indicaremos que nuestro proveedor cloud es AWS e indicaremos via una variable la region en la cual realizaremos
el despliegue.

```terraform
provider "aws" {
  region = var.region
}
```

Y definimos la variable en variables.tf

```terraform
variable "region" {
  description = "The AWS region to create resources in."
  default     = "us-east-1"
}
```

Y ahora definimos la red VPC, esto lo haremos en el fichero network.tf. De momento crearemos una vpc,
denominada **pbl-vpc**. Esta vpc tendra 2 subredes publicas y 2 subredes privadas (pensando en un futuro
para ofrecer HA. Ademas crearemos sus tablas de rutado, y crearemos un NatGateway (seria mas HA 2) con una IP elastica
y un Internet Gateway. Tambien creamos una ip elastica para asignarle al NAT.Estos recursos , para que sea algo mas flexibles , tienen los CIDR(rangos de ips) y los
Availability Zones definidos mediante variables en **variables.tf**.

```terraform
# Production VPC
resource "aws_vpc" "pbl-vpc" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_support   = true
  enable_dns_hostnames = true
}

# Public subnets
resource "aws_subnet" "public-subnet-1" {
  cidr_block        = var.public_subnet_1_cidr
  vpc_id            = aws_vpc.pbl-vpc.id
  availability_zone = var.availability_zones[0]
}
resource "aws_subnet" "public-subnet-2" {
  cidr_block        = var.public_subnet_2_cidr
  vpc_id            = aws_vpc.pbl-vpc.id
  availability_zone = var.availability_zones[1]
}

# Private subnets
resource "aws_subnet" "private-subnet-1" {
  cidr_block        = var.private_subnet_1_cidr
  vpc_id            = aws_vpc.pbl-vpc.id
  availability_zone = var.availability_zones[0]
}
resource "aws_subnet" "private-subnet-2" {
  cidr_block        = var.private_subnet_2_cidr
  vpc_id            = aws_vpc.pbl-vpc.id
  availability_zone = var.availability_zones[1]
}

# Route tables for the subnets
resource "aws_route_table" "public-route-table" {
  vpc_id = aws_vpc.pbl-vpc.id
}
resource "aws_route_table" "private-route-table" {
  vpc_id = aws_vpc.pbl-vpc.id
}

# Associate the newly created route tables to the subnets
resource "aws_route_table_association" "public-route-1-association" {
  route_table_id = aws_route_table.public-route-table.id
  subnet_id      = aws_subnet.public-subnet-1.id
}
resource "aws_route_table_association" "public-route-2-association" {
  route_table_id = aws_route_table.public-route-table.id
  subnet_id      = aws_subnet.public-subnet-2.id
}
resource "aws_route_table_association" "private-route-1-association" {
  route_table_id = aws_route_table.private-route-table.id
  subnet_id      = aws_subnet.private-subnet-1.id
}
resource "aws_route_table_association" "private-route-2-association" {
  route_table_id = aws_route_table.private-route-table.id
  subnet_id      = aws_subnet.private-subnet-2.id
}

# Elastic IP
resource "aws_eip" "elastic-ip-for-nat-gw" {
  vpc                       = true
  associate_with_private_ip = "10.0.0.5"
  depends_on                = [aws_internet_gateway.production-igw]
}

# NAT gateway
resource "aws_nat_gateway" "nat-gw" {
  allocation_id = aws_eip.elastic-ip-for-nat-gw.id
  subnet_id     = aws_subnet.public-subnet-1.id
  depends_on    = [aws_eip.elastic-ip-for-nat-gw]
}
resource "aws_route" "nat-gw-route" {
  route_table_id         = aws_route_table.private-route-table.id
  nat_gateway_id         = aws_nat_gateway.nat-gw.id
  destination_cidr_block = "0.0.0.0/0"
}

# Internet Gateway for the public subnet
resource "aws_internet_gateway" "production-igw" {
  vpc_id = aws_vpc.pbl-vpc.id
}

# Route the public subnet traffic through the Internet Gateway
resource "aws_route" "public-internet-igw-route" {
  route_table_id         = aws_route_table.public-route-table.id
  gateway_id             = aws_internet_gateway.production-igw.id
  destination_cidr_block = "0.0.0.0/0"
}
```

Y a continuacion definimos las variables que nos permitiran configurar las ips de las subredes,
junto con las zonas de disponibiliad.

```terraform
# variables networking

variable "public_subnet_1_cidr" {
  description = "CIDR Block for Public Subnet 1"
  default     = "10.0.1.0/24"
}
variable "public_subnet_2_cidr" {
  description = "CIDR Block for Public Subnet 2"
  default     = "10.0.2.0/24"
}
variable "private_subnet_1_cidr" {
  description = "CIDR Block for Private Subnet 1"
  default     = "10.0.3.0/24"
}
variable "private_subnet_2_cidr" {
  description = "CIDR Block for Private Subnet 2"
  default     = "10.0.4.0/24"
}
variable "availability_zones" {
  description = "Availability zones"
  type        = list(string)
  default     = ["us-east-1a", "us-east-1b"]
}
```

Tambien tenemos que definir los security groups para nuestras maquinas y servicios. Para facilitar el mantenimiento,
creamos los security groups en un fichero llamado **security_groups.tf**. De momento definiremos los security groups
para el load balancer que utilizaremos junto con ECS, abriendo el 8000 y el 80 , y despues otro security group para las tareas de ECS a las que
redirijira el trafico el ELB. Este segundo security group solo permitira trafico que haya pasado el security group de
ELB, es decir trafico que acepta el ELB;-)

```terraform
# ALB Security Group (Traffic Internet -> ALB)
resource "aws_security_group" "load-balancer" {
  name        = "load_balancer_security_group"
  description = "Controls access to ALB"
  ingress {
    from_port   = 8000
    to_port     = 8000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# ECS Security group (traffic ALB -> ECS)
resource "aws_security_group" "ecs" {
  name        = "ecs_security_group"
  description = "Allows inbound access from the ALB only"
  vpc_id      = aws_vpc.pbl-vpc.id

  ingress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    security_groups = [aws_security_group.load-balancer.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
```

Por ultimo definimos los outputs que queremos ver tras la creación de recursos. Para esto crearemos un fichero
llamado **outputs.tf**.Visualizaremos los id de las VPC, subredes y security group.
Despues estos valores los utilizaremos para desplegar el servicio Dockerizado en AWS ECS correctamente.

```terraform
output "vpc_id" {
  value = aws_vpc.pbl-vpc.id
}

output "ecs_security_group_id" {
  value = aws_security_group.ecs.id
}

output "load_balancer_security_group_id" {
  value = aws_security_group.load-balancer.id
}

output "public_subnet_1_id" {
  value = aws_subnet.public-subnet-1.id
}

output "public_subnet_2_id" {
  value = aws_subnet.public-subnet-2.id
}

output "private_subnet_1_id" {
  value = aws_subnet.private-subnet-1.id
}

output "private_subnet_2_id" {
  value = aws_subnet.private-subnet-2.id
}

```

Y ahora creamos la VPC;-)No olvideis actualizar las credenciales de AWS Academy;-)

```terraform
terraform init (para instalar el plugin AWS de Terraform)
terraform validate
terraform plan
  Plan: 18 to add, 0 to change, 0 to destroy.
terraform apply
    Apply complete! Resources: 18 added, 0 changed, 0 destroyed.

    Outputs:

    ecs_security_group_id = "sg-08f46158d483a5741"
    load_balancer_security_group_id = "sg-0e57e4d0e2bb36d6f"
    private_subnet_1_id = "subnet-0f51d79446510d2c5"
    private_subnet_2_id = "subnet-069a481bb3ec979ae"
    public_subnet_1_id = "subnet-030b1d18d55d91b85"
    public_subnet_2_id = "subnet-097bd9e3210f66c08"
    vpc_id = "vpc-015107dcfb8cb9162"
```

Como todavia no tenemos la aplicacion en esta VPC ni la base de datos RDS podemos destruir todo lo creado para no gastar
dinero (el NAT GATEWAY) , y ya volveremos a crear caundo nos haga falta;-)

```terraform
terraform destroy
```

En caso de querer actualizar solo un elemento del terraform podemos utilizar el parametro -target al hacer apply, esto es muy util cuando vamos haciendo cambios y no queremos destruir y crear todo otra vez (mas que nada por temas de tiempo de creacion de recursos;-)

```
terraform apply -target=aws_alb_listener.ecs-alb-http-listener
```
