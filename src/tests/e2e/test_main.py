import requests


# ToDo Parametrizar entorno de e22


class TestRoutes:
    def test_health_check(self, base_url):
        """
        GIVEN
        WHEN health check endpoint is called with GET method
        THEN response with status 200 and body OK is returned
        """
        # Given
        # client

        # When
        url = base_url + "/health-check/"
        print("" + url)
        response = requests.get(url)
        # Then
        assert response.status_code == 200
        assert response.json() == {
            "message": "OKv9",
            "debug": True,
            "environment": "development",
            "test": False
            # "database_url": "postgresql+asyncpg://postgres:postgres@web-db:5432/web_dev",
        }
