import pytest

from datetime import datetime

from services.domain.comments.Comments_infrastructure import (
    add_comment,
    get_comments,
    get_comment_by_id,
    delete_comment,
)
from sqlalchemy.future import select
from services.domain.comments.Comments_model import Comment


@pytest.mark.parametrize("anyio_backend", ["asyncio"])
async def test_db_use_adding_a_comment(test_db, anyio_backend):
    await add_comment(test_db, content="Comment for a post")
    statement = select(Comment).where(Comment.content == "Comment for a post")
    comments = await test_db.execute(statement)
    comment = comments.scalar()

    assert comment is not None
    assert comment.content == "Comment for a post"


@pytest.mark.parametrize("anyio_backend", ["asyncio"])
async def test_db_get_comments(test_db, anyio_backend):
    await add_comment(test_db, content="Content for the first comment")
    await add_comment(test_db, content="Content for the second comment")

    comments = await get_comments(test_db)
    assert len(comments) == 2


@pytest.mark.parametrize("anyio_backend", ["asyncio"])
async def test_db_get_comment(test_db, anyio_backend):
    test_data_comment = {
        "id": 1,
        "content": "foo.bar.1",
        "post_id": 1,
        "created_at": datetime.utcnow().isoformat(),
    }
    await add_comment(test_db, content=test_data_comment["content"])

    comment = await get_comment_by_id(test_db, 1)
    assert comment is not None
    assert comment["content"] == test_data_comment["content"]


@pytest.mark.parametrize("anyio_backend", ["asyncio"])
async def test_db_delete_comment(test_db, anyio_backend):
    test_data_comment = {
        "id": 1,
        "content": "foo.bar.1",
        "post_id": 1,
        "created_at": datetime.utcnow().isoformat(),
    }
    await add_comment(test_db, content=test_data_comment["content"])

    await delete_comment(test_db, 1)
    comment = await get_comment_by_id(test_db, 1)
    assert comment is None
