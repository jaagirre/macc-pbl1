import os

from services.config import load_config


def test_development_config(test_config_env):
    os.environ["APP_ENVIRONMENT"] = "development"
    app_settings = load_config()
    print("SQL_URL_SECRETS" + app_settings.SQLALCHEMY_DATABASE_URL)
    assert not app_settings.TESTING
    assert app_settings.DEBUG
    assert app_settings.APP_ENVIRONMENT == "development"


def test_test_config(test_config_env):
    os.environ["APP_ENVIRONMENT"] = "test"
    app_settings = load_config()
    assert app_settings.TESTING
    assert not app_settings.DEBUG
    assert app_settings.APP_ENVIRONMENT == "local"


def test_local_test_config(test_config_env):
    os.environ["APP_ENVIRONMENT"] = "local_test"
    app_settings = load_config()
    assert app_settings.TESTING
    assert app_settings.DEBUG
    assert app_settings.APP_ENVIRONMENT == "local"


def test_production_config(test_config_env):
    os.environ["APP_ENVIRONMENT"] = "production"
    app_settings = load_config()
    assert not app_settings.TESTING
    assert not app_settings.DEBUG
    assert app_settings.APP_ENVIRONMENT == "production"
