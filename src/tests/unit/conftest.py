import pytest
import os


@pytest.fixture
def test_config_env():
    app_env = os.environ["APP_ENVIRONMENT"]
    yield
    os.environ["APP_ENVIRONMENT"] = app_env  # esto hay qu ehacerlo con fitxure
