import pytest
from sqlalchemy.future import select

from services.domain.posts.Posts_infrastructure import (
    Post,
    add_post,
    get_posts,
    add_comment_to_post,
    get_post_by_id,
)


@pytest.mark.parametrize("anyio_backend", ["asyncio"])
async def test_db_use_adding_a_post(test_db, anyio_backend):
    await add_post(
        test_db, title="Post The First", content="Content for the first post"
    )
    statement = select(Post).where(Post.title == "Post The First")
    posts = await test_db.execute(statement)
    post = posts.scalar()
    # post = await test_db.get(Post).filter(Post.title == 'Post The First').first()
    assert post is not None
    assert post.title == "Post The First"


@pytest.mark.parametrize("anyio_backend", ["asyncio"])
async def test_db_get_posts(test_db, anyio_backend):
    await add_post(
        test_db, title="Post The First", content="Content for the first post"
    )
    await add_post(
        test_db, title="Post The First", content="Content for the first post"
    )

    posts = await get_posts(test_db)
    assert len(posts) == 2


# test que agrega un post y luego le agrega un  comentario
@pytest.mark.parametrize("anyio_backend", ["asyncio"])
async def test_add_comment_to_post(test_db, monkeypatch, anyio_backend):
    test_data_comment = {"content": "foo.bar.1"}
    test_data_post = {"id": 1, "title": "post.foo.bar.1", "content": "post.foo.bar.1"}
    await add_post(
        test_db, title=test_data_post["title"], content=test_data_post["content"]
    )
    await add_comment_to_post(test_db, 1, test_data_comment["content"])
    post = await get_post_by_id(test_db, 1)
    assert post is not None
    for comment in post.comments:
        print(f"> {comment.content}")
    assert post.comments[0].content == test_data_comment["content"]
