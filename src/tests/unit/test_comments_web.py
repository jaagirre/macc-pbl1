import pytest
import json
from datetime import datetime

import services.domain.comments.Comments_infrastructure as crud
import services.domain.posts.Posts_infrastructure as crud_posts

from services.domain.comments.Comments_model import CommentSchema
from services.domain.posts.Posts_model import PostSchema


@pytest.mark.parametrize("anyio_backend", ["asyncio"])
async def test_add_comment(test_client, monkeypatch, anyio_backend):
    test_data_comment = {
        "id": 1,
        "content": "foo.bar.1",
        "post_id": 0,
        "created_at": datetime.utcnow().isoformat(),
    }

    async def mock_add_comment(db, id):
        comment = CommentSchema.parse_obj(test_data_comment)
        return comment

    monkeypatch.setattr(crud, "add_comment", mock_add_comment)

    response = test_client.post(
        "/comments", data=json.dumps({"content": test_data_comment["content"]})
    )

    assert response.status_code == 201
    assert response.json() == test_data_comment


@pytest.mark.parametrize("anyio_backend", ["asyncio"])
async def test_add_comment_to_post(test_client, monkeypatch, anyio_backend):
    test_data_comment = {
        "id": 1,
        "content": "foo.bar.1",
        "post_id": 1,
        "created_at": datetime.utcnow().isoformat(),
    }
    test_data_post = {
        "id": 1,
        "title": "post.foo.bar.1",
        "content": "post.foo.bar.1",
        "comments": [],
        "created_at": datetime.utcnow().isoformat(),
    }

    async def mock_get_post_by_id(db, id):
        post = PostSchema.parse_obj(test_data_post)
        return post

    async def mock_add_comment(db, id, post):
        comment = CommentSchema.parse_obj(test_data_comment)
        return comment
        # aqui mockearmos las funciones que utiliza delete

    monkeypatch.setattr(crud_posts, "get_post_by_id", mock_get_post_by_id)
    monkeypatch.setattr(crud_posts, "add_comment_to_post", mock_add_comment)

    response = test_client.post(
        "/posts/1/comments",
        data=json.dumps(test_data_comment),
    )

    assert response.status_code == 201
    assert response.json() == test_data_comment


@pytest.mark.parametrize("anyio_backend", ["asyncio"])
async def test_remove_comment(test_client, monkeypatch, anyio_backend):
    test_data = {
        "id": 1,
        "content": "summary",
        "post_id": 1,
        "created_at": datetime.utcnow().isoformat(),
    }

    # definimos los mocks
    # comment = await crud.get_comment_by_id(db, id)

    async def mock_get_comment_by_id(db, id):
        return test_data

    # await crud.delete_comment(db, id)
    async def mock_delete_comment(db, id):
        return id

    # aqui mockearmos las funciones que utiliza delete
    monkeypatch.setattr(crud, "get_comment_by_id", mock_get_comment_by_id)
    monkeypatch.setattr(crud, "delete_comment", mock_delete_comment)

    comment_id = 1
    response = test_client.delete(f"/comments/{comment_id}/")
    assert response.status_code == 200
    assert response.json() == test_data


@pytest.mark.parametrize("anyio_backend", ["asyncio"])
async def test_remove_comment_incorrect_id(test_client, monkeypatch, anyio_backend):
    async def mock_get_comment_by_id(db, id):
        return None

    monkeypatch.setattr(crud, "get_comment_by_id", mock_get_comment_by_id)

    response = test_client.delete("/comments/9999/")
    assert response.status_code == 404
    assert response.json()["detail"] == "Comment not found"

    response = test_client.delete("/comments/0/")
    assert response.status_code == 422
    assert response.json() == {
        "detail": [
            {
                "loc": ["path", "id"],
                "msg": "ensure this value is greater than 0",
                "type": "value_error.number.not_gt",
                "ctx": {"limit_value": 0},
            }
        ]
    }


@pytest.mark.parametrize("anyio_backend", ["asyncio"])
async def test_read_comments(test_client, monkeypatch, anyio_backend):
    test_data = [
        {
            "id": 1,
            "content": "foo.bar.1",
            "post_id": 1,
            "created_at": datetime.utcnow().isoformat(),
        },
        {
            "id": 2,
            "content": "foo.bar.2",
            "post_id": 1,
            "created_at": datetime.utcnow().isoformat(),
        },
        {
            "id": 3,
            "content": "foo.bar.3",
            "post_id": 2,
            "created_at": datetime.utcnow().isoformat(),
        },
    ]

    async def mock_get_comments(db):
        return test_data

    monkeypatch.setattr(crud, "get_comments", mock_get_comments)
    response = test_client.get("/comments/")
    assert response.status_code == 200

    data = response.json()
    assert len(data) == 3
    assert "foo.bar.1" in data[0]["content"]
    assert "foo.bar.2" in data[1]["content"]
    assert "foo.bar.3" in data[2]["content"]


@pytest.mark.parametrize("anyio_backend", ["asyncio"])
async def test_read_post(test_client, monkeypatch, anyio_backend):
    test_data_post = {
        "id": 1,
        "title": "post.foo.bar.1",
        "content": "post.foo.bar.1",
        "comments": [
            {
                "id": 1,
                "content": "foo.bar.1",
                "post_id": 1,
                "created_at": datetime.utcnow().isoformat(),
            },
            {
                "id": 2,
                "content": "foo.bar.2",
                "post_id": 1,
                "created_at": datetime.utcnow().isoformat(),
            },
        ],
        "created_at": datetime.utcnow().isoformat(),
    }

    async def mock_get_post_by_id(db, id):
        post = PostSchema.parse_obj(test_data_post)
        return post

    monkeypatch.setattr(crud_posts, "get_post_by_id", mock_get_post_by_id)

    response = test_client.get("/posts/1/")
    assert response.status_code == 200

    response_dict = response.json()
    assert response_dict["id"] == test_data_post["id"]
    assert response_dict["title"] == test_data_post["title"]
    assert response_dict["content"] == test_data_post["content"]
    assert response_dict["created_at"] == test_data_post["created_at"]
    # ToDo
    assert response_dict["comments"] == test_data_post["comments"]
