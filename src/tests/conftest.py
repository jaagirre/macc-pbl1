# ToDo: Migrar este fichero a config.py
import os

import pytest
from starlette.testclient import TestClient

from services.config import load_config
from services.db import Base as SQLModel
from services.db import (
    async_session_generator,
    create_db_engine,
    drop_db,
    get_db,
    reset_db,
)
from services.main import create_app

# ToDo: Crear un ORM no async y crear mismo test y comprobar tiempos de ejecucion


async def get_db_test_override():
    app_env = os.environ["APP_ENVIRONMENT"]
    os.environ["APP_ENVIRONMENT"] = "local_test"
    app_config = load_config()
    db_engine = create_db_engine(app_config)
    SessionLocal = async_session_generator(db_engine)
    # SessionLocal = async_session_generator()
    print("Getting database SessionLocal")
    db = SessionLocal()
    try:
        yield db
        await db.commit()
    except Exception:
        await db.rollback()
    finally:
        await db.close()
        os.environ["APP_ENVIRONMENT"] = app_env


# Hemen fixture desberdinak. Tanto para el que va solo
# Como los demas , para tener base de datos con datos diferentes
@pytest.fixture
async def test_db():
    app_env = os.environ["APP_ENVIRONMENT"]
    os.environ["APP_ENVIRONMENT"] = "local_test"
    app_config = load_config()
    print("SQLALCHEMY_DATABASE_URL:" + app_config.SQLALCHEMY_DATABASE_URL)
    db_engine = create_db_engine(app_config)
    # Hemendik gorakoa kendu daiteke
    # await reset_db(async_engine, SQLModel)
    await reset_db(db_engine, SQLModel)

    SessionLocal = async_session_generator(db_engine)
    # SessionLocal = async_session_generator()
    print("Getting database SessionLocal")
    db = SessionLocal()
    try:
        yield db  # Lo mantengo para los que lo utilizan
        await db.commit()
    except Exception:
        await db.rollback()
    finally:
        await db.close()
        await drop_db(db_engine, SQLModel)
        os.environ["APP_ENVIRONMENT"] = app_env
        # await drop_db(async_engine, SQLModel)


@pytest.fixture
def test_client():
    # En este caso asegurar con las varibales de entorno la configuracion correcta ;-)
    app = create_app()
    app.dependency_overrides[
        get_db
    ] = get_db_test_override  # esto lo hago porque al tener dos gestiones
    # de base de datos y una hace destroy  y la otra no crea
    with TestClient(app) as test_client:
        # testing
        yield test_client


"""
@pytest.fixture
async def test_db_ona():
    await reset_db()
    db = async_session_generator()

    async with db() as session:
        async with async_engine.begin() as conn:
            await conn.run_sync(SQLModel.metadata.create_all)
        try:
            yield session
            await session.commit()
        except:
            await session.rollback()
        finally:
            await session.close()
            async with async_engine.begin() as conn:
                await conn.run_sync(SQLModel.metadata.drop_all)




"""
