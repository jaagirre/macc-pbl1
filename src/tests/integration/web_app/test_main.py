import pytest
from starlette.testclient import TestClient

from services.config import load_config
from services.main import create_app


def override_load_config():
    # Creamos configuracion local
    app_config = load_config()
    app_config.APP_ENVIRONMENT = "local"
    app_config.SQLALCHEMY_DATABASE_URL = (
        "postgresql+asyncpg://postgres:postgres@web-db:5432/web_dev"
    )
    app_config.TESTING = False
    return app_config


@pytest.fixture(scope="module")
def client():
    # injectamos configuracion ;-)
    app = create_app()
    app.dependency_overrides[load_config] = override_load_config
    with TestClient(app) as test_client:
        # testing
        yield test_client


class TestRoutes:
    def test_health_check(self, client):
        """
        GIVEN
        WHEN health check endpoint is called with GET method
        THEN response with status 200 and body OK is returned
        """
        # Given
        # client

        # When
        response = client.get("/health-check/")
        # Then
        assert response.status_code == 200
        assert response.json() == {
            "message": "OKv9",
            "debug": True,
            "environment": "local",
            "test": False
            # "database_url": "postgresql+asyncpg://postgres:postgres@web-db:5432/web_dev",
        }
