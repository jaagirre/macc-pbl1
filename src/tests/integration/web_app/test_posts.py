import json

import pytest


@pytest.mark.parametrize("anyio_backend", ["asyncio"])
async def test_create_post(test_db, test_client, anyio_backend):
    response = test_client.post(
        "/posts/",
        data=json.dumps(
            {"title": "Example Test Post 1", "content": "Example Test Post 1 content"}
        ),
    )

    assert response.status_code == 201
    assert "Example Test Post 1" in response.json()["title"]
    assert "Example Test Post 1 content" in response.json()["content"]


@pytest.mark.parametrize("anyio_backend", ["asyncio"])
async def test_create_post_invalid_json(test_db, test_client, anyio_backend):
    response = test_client.post("/posts/", data=json.dumps({}))

    assert response.status_code == 422
    assert response.json() == {
        "detail": [
            {
                "loc": ["body", "title"],
                "msg": "field required",
                "msg": "field required",
                "type": "value_error.missing",
            },
            {
                "loc": ["body", "content"],
                "msg": "field required",
                "type": "value_error.missing",
            },
        ]
    }


@pytest.mark.parametrize("anyio_backend", ["asyncio"])
def test_read_post(test_db, test_client, anyio_backend):
    response = test_client.post(
        "/posts/", data=json.dumps({"title": "foo.bar", "content": "foo.bar"})
    )
    post_id = response.json()["id"]

    response = test_client.get(f"/posts/{post_id}/")
    assert response.status_code == 200

    response_dict = response.json()
    assert response_dict["id"] == post_id
    assert response_dict["title"] == "foo.bar"
    assert response_dict["content"] == "foo.bar"
    assert response_dict["created_at"]


@pytest.mark.parametrize("anyio_backend", ["asyncio"])
def test_read_post_incorrect_id(test_db, test_client, anyio_backend):
    response = test_client.get("/posts/9999/")
    assert response.status_code == 404

    response_dict = response.json()
    assert response_dict["detail"] == "Post not found"

    response = test_client.get("/posts/0/")
    assert response.status_code == 422
    assert response.json() == {
        "detail": [
            {
                "loc": ["path", "id"],
                "msg": "ensure this value is greater than 0",
                "type": "value_error.number.not_gt",
                "ctx": {"limit_value": 0},
            }
        ]
    }


@pytest.mark.parametrize("anyio_backend", ["asyncio"])
def test_read_posts(test_db, test_client, anyio_backend):
    response = test_client.post(
        "/posts/", data=json.dumps({"title": "foo.bar.1", "content": "foo.bar.1"})
    )

    assert response.status_code == 201
    response = test_client.post(
        "/posts/", data=json.dumps({"title": "foo.bar.2", "content": "foo.bar.2"})
    )
    assert response.status_code == 201

    response = test_client.get("/posts/")
    assert response.status_code == 200

    data = response.json()
    assert len(data) == 2
    # esto luego hay que cambiarlo de orden
    assert "foo.bar.1" in data[1]["title"]
    assert "foo.bar.1" in data[1]["content"]
    assert "foo.bar.2" in data[0]["title"]
    assert "foo.bar.2" in data[0]["content"]


@pytest.mark.parametrize("anyio_backend", ["asyncio"])
def test_remove_summary(test_db, test_client, anyio_backend):
    response = test_client.post(
        "/posts/", data=json.dumps({"title": "foo.bar", "content": "foo.bar"})
    )
    post_id = response.json()["id"]

    response = test_client.delete(f"/posts/{post_id}/")
    assert response.status_code == 200
    assert response.json() == {"id": post_id, "title": "foo.bar", "content": "foo.bar"}


@pytest.mark.parametrize("anyio_backend", ["asyncio"])
def test_remove_post_incorrect_id(test_db, test_client, anyio_backend):
    response = test_client.delete("/posts/9999/")
    assert response.status_code == 404
    assert response.json()["detail"] == "Post not found"

    response = test_client.delete("/posts/0/")
    assert response.status_code == 422
    assert response.json() == {
        "detail": [
            {
                "loc": ["path", "id"],
                "msg": "ensure this value is greater than 0",
                "type": "value_error.number.not_gt",
                "ctx": {"limit_value": 0},
            }
        ]
    }


@pytest.mark.parametrize("anyio_backend", ["asyncio"])
def test_update_post(test_db, test_client, anyio_backend):
    response = test_client.post(
        "/posts/", data=json.dumps({"title": "foo.bar", "content": "foo.bar"})
    )
    post_id = response.json()["id"]

    response = test_client.put(
        f"/posts/{post_id}/",
        data=json.dumps({"title": "foo.bar", "content": "updated!"}),
    )
    assert response.status_code == 200

    response_dict = response.json()
    assert response_dict["id"] == post_id
    assert response_dict["title"] == "foo.bar"
    assert response_dict["content"] == "updated!"
    assert response_dict["created_at"]


@pytest.mark.parametrize("anyio_backend", ["asyncio"])
def test_update_post_incorrect_id(test_db, test_client, anyio_backend):
    response = test_client.post(
        "/posts/", data=json.dumps({"title": "foo.bar", "content": "foo.bar"})
    )
    response.json()["id"]

    response = test_client.put(
        "/posts/9999/", data=json.dumps({"title": "foo.bar", "content": "updated!"})
    )
    assert response.status_code == 404
    assert response.json()["detail"] == "Post not found"

    response = test_client.put(
        "/posts/0/", data=json.dumps({"title": "foo.bar", "content": "updated!"})
    )
    assert response.status_code == 422
    assert response.json() == {
        "detail": [
            {
                "loc": ["path", "id"],
                "msg": "ensure this value is greater than 0",
                "type": "value_error.number.not_gt",
                "ctx": {"limit_value": 0},
            }
        ]
    }


@pytest.mark.parametrize("anyio_backend", ["asyncio"])
def test_update_post_invalid_json(test_db, test_client, anyio_backend):
    response = test_client.post(
        "/posts/", data=json.dumps({"title": "foo.bar", "content": "foo.bar"})
    )
    post_id = response.json()["id"]

    response = test_client.put(f"/posts/{post_id}/", data=json.dumps({}))
    assert response.status_code == 422
    assert response.json() == {
        "detail": [
            {
                "loc": ["body", "title"],
                "msg": "field required",
                "type": "value_error.missing",
            },
            {
                "loc": ["body", "content"],
                "msg": "field required",
                "type": "value_error.missing",
            },
        ]
    }


@pytest.mark.parametrize("anyio_backend", ["asyncio"])
def test_update_post_invalid_key(test_db, test_client, anyio_backend):
    response = test_client.post(
        "/posts/", data=json.dumps({"title": "foo.bar", "content": "foo.bar"})
    )
    post_id = response.json()["id"]

    response = test_client.put(
        f"/posts/{post_id}/", data=json.dumps({"title": "new title"})
    )
    assert response.status_code == 422
    assert response.json() == {
        "detail": [
            {
                "loc": ["body", "content"],
                "msg": "field required",
                "type": "value_error.missing",
            }
        ]
    }


@pytest.mark.parametrize(
    "post_id, payload, status_code, detail, anyio_backend",
    [
        [
            999,
            {"title": "foo.bar", "content": "updated!"},
            404,
            "Post not found",
            "asyncio",
        ],
        [
            0,
            {"title": "foo.bar", "content": "updated!"},
            422,
            [
                {
                    "loc": ["path", "id"],
                    "msg": "ensure this value is greater than 0",
                    "type": "value_error.number.not_gt",
                    "ctx": {"limit_value": 0},
                }
            ],
            "asyncio",
        ],
        [
            1,
            {},
            422,
            [
                {
                    "loc": ["body", "title"],
                    "msg": "field required",
                    "type": "value_error.missing",
                },
                {
                    "loc": ["body", "content"],
                    "msg": "field required",
                    "type": "value_error.missing",
                },
            ],
            "asyncio",
        ],
        [
            1,
            {"title": "foo.bar"},
            422,
            [
                {
                    "loc": ["body", "content"],
                    "msg": "field required",
                    "type": "value_error.missing",
                }
            ],
            "asyncio",
        ],
    ],
)
def test_update_post_invalid(
    test_db, test_client, post_id, payload, status_code, detail, anyio_backend
):
    response = test_client.put(f"/posts/{post_id}/", data=json.dumps(payload))
    assert response.status_code == status_code
    assert response.json()["detail"] == detail
