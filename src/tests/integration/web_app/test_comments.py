import pytest
import json


@pytest.mark.parametrize("anyio_backend", ["asyncio"])
async def test_add_comment(test_db, test_client, monkeypatch, anyio_backend):
    test_data_comment = {"id": 1, "content": "foo.bar.1", "post_id": 0}
    response = test_client.post(
        "/comments",
        data=json.dumps({"content": test_data_comment["content"]}),
    )

    assert response.status_code == 201
    assert response.json()["content"] == test_data_comment["content"]


@pytest.mark.parametrize("anyio_backend", ["asyncio"])
async def test_add_comment_to_post(test_db, test_client, monkeypatch, anyio_backend):
    test_data_comment = {
        "content": "foo.bar.1",
    }
    # priemro agregamos el post
    response = test_client.post(
        "/posts/",
        data=json.dumps(
            {"title": "post.foo.bar.1", "content": "post.content.foo.bar.1"}
        ),
    )
    assert response.status_code == 201
    post_id = response.json()["id"]
    # y ahora agregamos el comment
    response = test_client.post(
        f"/posts/{post_id}/comments",
        data=json.dumps(test_data_comment),
    )

    assert response.status_code == 201
    assert response.json()["content"] == test_data_comment["content"]
    assert response.json()["post_id"] == post_id

    response = test_client.get("/posts/")
    assert response.status_code == 200

    data = response.json()
    assert len(data) == 1
    assert "foo.bar.1" in data[0]["title"]
    assert "foo.bar.1" in data[0]["content"]
