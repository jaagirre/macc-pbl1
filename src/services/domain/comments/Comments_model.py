from datetime import datetime
from sqlalchemy import Column, DateTime, Integer, Text, ForeignKey
from pydantic import BaseModel

from services.db import Base
from sqlalchemy.orm import relationship


class CommentPayloadSchema(BaseModel):
    content: str


class CommentSchema(CommentPayloadSchema):
    id: int
    post_id: int
    created_at: datetime


class Comment(Base):
    __tablename__ = "Comments"
    id = Column(Integer, primary_key=True)
    content = Column(Text)
    post_id = Column(Integer, ForeignKey("Posts.id"))
    created_at = Column(DateTime, index=True, default=datetime.utcnow)
    post = relationship("Post", back_populates="comments")

    def __repr__(self):
        return f'<Comment "{self.content[:20]}">'
