import logging


from sqlalchemy.ext.asyncio import AsyncSession

from sqlalchemy.future import select


from services.domain.comments.Comments_model import Comment

log = logging.getLogger(__name__)


async def add_comment(db: AsyncSession, content: str):
    comment = Comment(content=content)
    try:
        db.add(comment)  # post.save erabiltzen dau
        await db.commit()
        await db.refresh(comment)
    except Exception as exc:
        log.error(exc)
    return comment


async def get_comments(db: AsyncSession):
    statement = select(Comment)
    comments = await db.execute(statement)
    comments = comments.scalars().all()
    comments_as_dict = []

    comments_as_dict = [c.__dict__ for c in comments]

    return comments_as_dict


async def get_comment_by_id(db: AsyncSession, id):
    comment = await _get_comment_by_id(db, id)
    if comment is not None:
        comment = comment.__dict__
    return comment


async def _get_comment_by_id(db: AsyncSession, id):
    statement = select(Comment).where(Comment.id == id)
    comments = await db.execute(statement)

    if not comments:
        comment = None
    else:
        query_comment = comments.scalar()
        if query_comment is not None:
            comment = query_comment
        else:
            comment = None
    return comment


async def delete_comment(db, id):
    # ToDo : Delete any DB element by id.
    element = await _get_comment_by_id(
        db, id
    )  # Se puede generalizar con una funcion que recibia el tipo de modelo alchemy. Ya que solo cambie el select
    if element is not None:
        await db.delete(element)
        await db.commit()
        element
    return element


"""
comment = Comment(content=request.form['content'], post=post)
    dbSession.add(comment)
    dbSession.commit()
"""
