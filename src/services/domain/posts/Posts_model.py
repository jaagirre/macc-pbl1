from datetime import datetime
from typing import List

from pydantic import BaseModel
from sqlalchemy import Column, DateTime, Integer, String, Text
from sqlalchemy.orm import relationship

from services.db import Base
from services.domain.comments.Comments_model import CommentSchema


class PostPayloadSchema(BaseModel):
    title: str
    content: str


class PostResponseSchema(PostPayloadSchema):
    id: int


class PostSchema(PostResponseSchema):
    created_at: datetime
    comments: List[CommentSchema]


class Post(Base):
    __tablename__ = "Posts"
    id = Column(Integer, primary_key=True)
    title = Column(String(100))
    content = Column(Text)
    comments = relationship("Comment", back_populates="post")
    created_at = Column(DateTime, index=True, default=datetime.utcnow)

    def __repr__(self):
        return f'<Post "{self.title}">'
