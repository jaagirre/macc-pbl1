import logging

from sqlalchemy import update as sqlalchemy_update
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.future import select

from services.domain.posts.Posts_model import Post
from services.domain.comments.Comments_model import Comment, CommentSchema
from sqlalchemy.orm import selectinload

log = logging.getLogger(__name__)


# ToDo Convertir en una clase abstracta e implementacion


async def get_posts(db: AsyncSession):
    # stmt = select(Post)
    stmt = select(Post).join(Comment, isouter=True).options(selectinload(Post.comments))
    posts = await db.execute(stmt)
    posts = posts.scalars().all()
    posts_as_dict = []

    posts_as_dict = [p.__dict__ for p in posts]
    for p in posts_as_dict:
        p["comments"] = [CommentSchema.parse_obj(c.__dict__) for c in p["comments"]]

    return posts_as_dict


async def get_post_by_id(db: AsyncSession, post_id: int):
    # stmt = select(Post).where(Post.id == post_id)
    stmt = (
        select(Post)
        .join(Comment, isouter=True)
        .where(Post.id == post_id)
        .options(selectinload(Post.comments))
    )
    posts = await db.execute(stmt)
    query_post = posts.scalar()
    if query_post is not None:
        post = query_post
    else:
        post = None
    return post


async def get_post_by_title(db: AsyncSession, title):
    statement = select(Post).where(Post.title == title)
    posts = await db.execute(statement)
    post = posts.scalar()
    return post


async def add_post(db: AsyncSession, title: str, content: str):
    post = Post(title=title, content=content)  # , comments=[])
    """Insert a role in the database."""

    try:
        db.add(post)  # post.save erabiltzen dau
        await db.commit()
        await db.refresh(post)
    except Exception as exc:
        log.error(exc)
    return post


async def update_post(db: AsyncSession, id: int, content: str):
    """ToDO: Delete any DB element by id."""
    # post = await get_post_by_id(db, id)
    # await User.update(id, full_name=full_name)

    query = (
        sqlalchemy_update(Post)
        .where(Post.id == id)
        .values(content=content)
        .execution_options(synchronize_session="fetch")
    )

    await db.execute(query)
    await db.commit()
    element = await get_post_by_id(db, id)
    return element


async def delete_post(db: AsyncSession, id: int):
    """ToDo : Delete any DB element by id."""
    element = await get_post_by_id(
        db, id
    )  # Se puede generalizar con una funcion que recibia el tipo de modelo alchemy. Ya que solo cambie el select
    if element is not None:
        await db.delete(element)
        await db.commit()
        element
    return element


async def add_comment_to_post(db: AsyncSession, post_id: int, comment_text):
    post = await get_post_by_id(db, post_id)
    if post is not None:
        comment = Comment(content=comment_text)
        db.add(comment)
        post.comments.append(comment)
        await db.commit()
        await db.refresh(post)
        await db.refresh(comment)
    else:
        comment = None
    return comment
