import logging

from fastapi import FastAPI

# # configuramos
import services.db as db
from services.routers.comments import router as router_comments
from services.routers.healthcheck import router as router_healthcheck
from services.routers.posts import router as router_posts

log = logging.getLogger("uvicorn")


def create_app() -> FastAPI:
    server = FastAPI()

    server.include_router(router_healthcheck)
    server.include_router(router_posts, prefix="/posts", tags=["posts"])
    server.include_router(router_comments, prefix="/comments", tags=["comments"])

    return server


app = create_app()


@app.on_event("startup")
async def startup_event():
    log.info("Starting up...")
    # from  services.db import Base as SQLMOdel # Esto es debido a que tenemos desacoplado
    await db.init_sql_model()


@app.on_event("shutdown")
async def shutdown_event():
    log.info("Shutting down...")
