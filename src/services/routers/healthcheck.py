from fastapi import APIRouter, Depends
from sqlalchemy.ext.asyncio import AsyncSession

from services.config import BaseConfig, load_config
from services.db import get_session

router = APIRouter()


@router.get("/health-check/")
async def healthcheck(
    config: BaseConfig = Depends(load_config), db: AsyncSession = Depends(get_session)
):
    return {
        "message": "OKv9",
        "environment": config.APP_ENVIRONMENT,
        "debug": config.DEBUG,
        "test": config.TESTING
        # "database_url": config.SQLALCHEMY_DATABASE_URL,
    }
