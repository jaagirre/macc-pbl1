from typing import List

from fastapi import APIRouter, Depends, HTTPException, Path
from sqlalchemy.ext.asyncio import AsyncSession

import services.domain.posts.Posts_infrastructure as crud
from services.db import get_db
from services.domain.posts.Posts_model import (
    PostPayloadSchema,
    PostResponseSchema,
    PostSchema,
)


from services.domain.comments.Comments_model import CommentSchema, CommentPayloadSchema

router = APIRouter()


@router.post("/", response_model=PostResponseSchema, status_code=201)
async def create_post(payload: PostPayloadSchema, db: AsyncSession = Depends(get_db)):
    post = await crud.add_post(
        db, payload.title, payload.content
    )  # el await lo hace dentro. Y aqui igual seria mejor pasar directamente el payload
    response_object = {"id": post.id, "title": post.title, "content": post.content}
    return response_object


@router.get("/", response_model=List[PostSchema])
async def read_posts(db: AsyncSession = Depends(get_db)):
    posts = await crud.get_posts(db)
    return posts


@router.get("/{id}/", response_model=PostSchema)
async def read_post(
    id: int = Path(..., gt=0), db: AsyncSession = Depends(get_db)
) -> PostSchema:
    post = await crud.get_post_by_id(db, id)
    if post is None:
        raise HTTPException(status_code=404, detail="Post not found")
    return post.__dict__


@router.delete("/{id}/", response_model=PostResponseSchema)
async def delete_summary(
    db: AsyncSession = Depends(get_db), id: int = Path(..., gt=0)
) -> PostResponseSchema:
    post = await crud.get_post_by_id(db, id)
    if not post:
        raise HTTPException(status_code=404, detail="Post not found")

    await crud.delete_post(db, id)

    return post.__dict__


@router.put("/{id}/", response_model=PostSchema)
async def update_post(
    payload: PostPayloadSchema,
    db: AsyncSession = Depends(get_db),
    id: int = Path(..., gt=0),
) -> PostResponseSchema:
    post = await crud.update_post(db, id, payload.content)
    if not post:
        raise HTTPException(status_code=404, detail="Post not found")

    return post.__dict__


@router.post("/{id}/comments", response_model=CommentSchema, status_code=201)
async def create_comment(
    payload: CommentPayloadSchema,
    db: AsyncSession = Depends(get_db),
    id: int = Path(..., gt=0),
):
    post = await crud.get_post_by_id(db, id)
    if not post:
        raise HTTPException(status_code=404, detail="Post not found")
    comment = await crud.add_comment_to_post(db, post.id, payload.content)

    response_object = {
        "id": comment.id,
        "content": comment.content,
        "post_id": comment.post_id,
        "created_at": comment.created_at,
    }
    return response_object
