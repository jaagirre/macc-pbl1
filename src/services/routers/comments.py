from typing import List
from fastapi import APIRouter, Depends, HTTPException, Path
from sqlalchemy.ext.asyncio import AsyncSession

import services.domain.comments.Comments_infrastructure as crud


from services.domain.comments.Comments_model import CommentSchema, CommentPayloadSchema
from services.db import get_db

router = APIRouter()


@router.post("/", response_model=CommentSchema, status_code=201)
async def create_post(
    payload: CommentPayloadSchema, db: AsyncSession = Depends(get_db)
):
    comment = await crud.add_comment(db, payload.content)
    response_object = {
        "id": comment.id,
        "content": comment.content,
        "post_id": 0,
        "created_at": comment.created_at,
    }
    return response_object


@router.delete("/{id}/", response_model=CommentSchema)
async def delete_comment(
    db: AsyncSession = Depends(get_db), id: int = Path(..., gt=0)
) -> CommentSchema:
    comment = await crud.get_comment_by_id(db, id)
    if not comment:
        raise HTTPException(status_code=404, detail="Comment not found")

    await crud.delete_comment(db, id)

    return comment


@router.get("/", response_model=List[CommentSchema])
async def read_comments(db: AsyncSession = Depends(get_db)):
    posts = await crud.get_comments(db)
    return posts
