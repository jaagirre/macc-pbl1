from contextlib import asynccontextmanager

from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.pool import NullPool

from services.config import load_config

# En ejecucion obtendra la configuracion den env, mientras que en los test el tester tendra que encargarse de esto
# ToDo: al utilizar esto en test no hay variables de entorno , por lo tanto por defecto será NONE


def create_db_engine(app_config):
    db_engine = create_async_engine(
        app_config.SQLALCHEMY_DATABASE_URL, echo=True, future=True, poolclass=NullPool
    )
    return db_engine


def async_session_generator(db_engine):
    return sessionmaker(db_engine, class_=AsyncSession)


config = load_config()
engine = create_db_engine(config)


@asynccontextmanager
async def get_session():  # este mete  con depends al llamarlo . Es singleton?
    try:
        async_session = async_session_generator(engine)  # Esto lo he llevado fuera

        async with async_session() as session:
            yield session
    except Exception:
        await session.rollback()
        raise
    finally:
        await session.close()


# Database #########################################################################################
async def get_db():
    """Generates database sessions and closes them when finished."""
    SessionLocal = async_session_generator(engine)
    print("Getting database SessionLocal")
    db = SessionLocal()
    try:
        yield db
        await db.commit()
    except Exception:
        await db.rollback()
    finally:
        await db.close()


"""
async def get_db():
    SessionLocal = async_session_generator()
    print("Getting database SessionLocal")
    db = SessionLocal()
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)
    try:
        yield db
        await db.commit()
    except:
        await db.rollback()
    finally:
        await db.close()
"""

Base = declarative_base()

# esta linea se puede comentar y funciona bien , pero me genera dudas. en flask y para alembic  ogual nos hace falta.
# esta por ver
from services.domain.posts.Posts_model import Post  # noqa: E402, F401


async def init_sql_model():
    db = async_session_generator(engine)

    async with db() as session:
        async with engine.begin() as conn:
            await conn.run_sync(Base.metadata.create_all)
        try:
            await session.commit()
        except Exception:
            await session.rollback()
        finally:
            await session.close()


async def drop_db(db_engine, sql_model):
    async with db_engine.begin() as conn:
        await conn.run_sync(sql_model.metadata.drop_all)


async def reset_db(db_engine, sql_model):
    async with db_engine.begin() as conn:
        await conn.run_sync(sql_model.metadata.drop_all)
        await conn.run_sync(sql_model.metadata.create_all)
