# project/config.py
import os

import boto3

# ToDo: Migrar configuracion entornos a pydantic BaseSettings
#  https://rednafi.github.io/digressions/python/2020/06/03/python-configs.html
#  https://fastapi.tiangolo.com/es/advanced/settings/


class BaseConfig:
    DEBUG = False
    TESTING = False
    APP_ENVIRONMENT = "production"
    _SQLALCHEMY_DATABASE_URL = None


# entorno produccion
class ProductionConfig(BaseConfig):
    _SQLALCHEMY_DATABASE_URL = ""

    @property
    def SQLALCHEMY_DATABASE_URL(self):
        if self._SQLALCHEMY_DATABASE_URL is None:
            self._SQLALCHEMY_DATABASE_URL = boto3.client(
                "secretsmanager"
            ).get_secret_value(SecretId=f"db-connection-string-{self.APP_ENVIRONMENT}")[
                "SecretString"
            ]

        return self._SQLALCHEMY_DATABASE_URL


# cccnmn podria ser uan especie de entorno staging
class DevelopmentConfig(ProductionConfig):
    DEBUG = True
    APP_ENVIRONMENT = "development"
    # SQLALCHEMY_DATABASE_URL = os.environ.get("DATABASE_DEV_URL")
    _SQLALCHEMY_DATABASE_URL = None

    @property
    def SQLALCHEMY_DATABASE_URL(self):
        if self._SQLALCHEMY_DATABASE_URL is None:
            self._SQLALCHEMY_DATABASE_URL = boto3.client(
                "secretsmanager"
            ).get_secret_value(SecretId=f"db-connection-string-{self.APP_ENVIRONMENT}")[
                "SecretString"
            ]

        return self._SQLALCHEMY_DATABASE_URL


# Entorno CI/CD  pipeline (docker)
class TestConfig(BaseConfig):
    TESTING = True
    DEBUG = False
    APP_ENVIRONMENT = "local"  # iguel hobeto pipeline/cI
    SQLALCHEMY_DATABASE_URL = os.environ.get("DATABASE_TEST_URL")


# Entorno desarrollo local (docker-compose)
class LocalTestConfig(BaseConfig):
    DEBUG = True
    APP_ENVIRONMENT = "local"
    SQLALCHEMY_DATABASE_URL = os.environ.get("DATABASE_LOCAL_TEST_URL")
    TESTING = True


# Entorno Local
class LocalConfig(BaseConfig):
    APP_ENVIRONMENT = "local"
    SQLALCHEMY_DATABASE_URL = os.environ.get("DATABASE_LOCAL_URL")
    DEBUG = True


CONFIGS = {
    "production": ProductionConfig,
    "development": DevelopmentConfig,
    "test": TestConfig,
    "local_test": LocalTestConfig,
    "local": LocalConfig,
}


def load_config() -> BaseConfig:
    """
    Load config based on environment
    :return:
    """

    return CONFIGS.get(os.getenv("APP_ENVIRONMENT"), LocalConfig)()


# app_config = load_config()
