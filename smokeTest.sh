#!/bin/bash
#ToDo paremetrizar normbre servicio
target_group_arn=`aws ecs describe-services --cluster fastapi-tdd-dev  --services fastapi-tdd-dev  --region us-east-1 | jq .services[].loadBalancers[].targetGroupArn|  tr -d '"'`
elb_arn=`aws elbv2  describe-target-groups --target-group-arn ${target_group_arn} --region us-east-1| jq .TargetGroups[].LoadBalancerArns[] | tr -d '"'`
elb_ip=`aws elbv2 describe-load-balancers --load-balancer-arns ${elb_arn} --region us-east-1 | jq .LoadBalancers[].DNSName | tr -d '"'`
echo $elb_ip
echo "Waiting for ECS task update..."
aws ecs wait services-stable --cluster fastapi-tdd-dev --services fastapi-tdd-dev --region us-east-1
url="http://${elb_ip}:8000/health-check/"
echo $url
curl -X 'GET' \
   $url\
  -H 'accept: application/json'

elb_url="http://${elb_ip}:8000"
echo $elb_url
cd project/tests/e2e
pytest  -p no:warnings --base-url=$elb_url -vv



