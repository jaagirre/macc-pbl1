# Paso 13 (OPCIONAL): Creando Terraform para reproducir fácilmente el entorno de producción y  controlar mejor el gasto en AWS Academy

En este caso utilizaremos Terraform para gestion de la infrastructura. Primero los instalaremos.
```bash
wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
sudo apt update && sudo apt install terraform
terraform -v
  Terraform v1.3.7
  on linux_amd64
```

Una vez instalado creamos la carpeta **infrastructure**, en la cual alojaremos  el código que gestionara la infrastructura.
Dentro de esta crearemos la carpeta **global**, y sobre esta **vpc**.Y en la carpeta **vpc** creamos el fichero main.tf.
De momento solo indicamos en este fichero que el backend que utilizaremos para guardar el estado de la infrastructura 
será local. Mas adelante, si incluimos la infraestructura en el pipeline, pasaremos gestionar el backend desde gitlab via http. 

```terraform
terraform{
  backend "local"{}
}
```

Y ahora inicializamos terraform desde la carpeta **vpc**, haciendo simplemente init. 
Y vemos como esto crea la carpeta **.terraform** donde el backend local guardara el estado.


```bash
cd infrastructure/global/vpc/
terraform init
  Initializing the backend...
ls -la
  -rw-rw-r-- 1 main.tf
  drwxr-xr-x 2 .terraform
```

Y ahora, aunque todavia no hayamos definido recursos, comenzamos con el flujo clasico de terraform:validate, plan y apply.
```bash
terraform validate
  Success! The configuration is valid.
terraform plan
  No changes. Your infrastructure matches the configuration.
terraform apply
  Apply complete! Resources: 0 added, 0 changed, 0 destroyed.
```

El estado no lo subiremos al repo por lo que añadimos al .gitignore esta carpeta y los ficheros de estado.
```bash
# Local .terraform directories
**/.terraform/*

# .tfstate files
*.tfstate
*.tfstate.*
```

Ya estamos preparados para comenzar a crear nuestra infraestructura. 
En este caso empezaremos creando primero la vpc sobre la que haremos el despliegue de ECS y RDS.


13.1- VPC
La VPC que crearemos tendra  2 subredes publicas y 2 subredes privadas, desplegadas en 2 zonas de disponibilidad.
Las aplicaciones , en este caso la aplicación dockerizada la ubicaremos en las subredes privadas, al igual que 
la base de datos.Para dar salida a internet a los elementos ubicados en  las subredes privadas tendremos un servicio NAT para dar salida a 
internet con ip publica a los elementos con  ip privadas. Tambien tendremos un Internet gateway.Ademas de ests elementso crearemos las tbalas de rutado correspondientes, security groups y acls.


En **main.tf** indicaremos que nuestro proveedor cloud es AWS e indicaremos via una variable la region en la cual realizaremos
el despliegue.

```terraform
provider "aws" {
  region = var.region
}
```
Y definimos la variable en variables.tf


```terraform
variable "region" {
  description = "The AWS region to create resources in."
  default     = "us-east-1"
}
```

Y ahora definimos la red VPC, esto lo haremos en el fichero network.tf. De momento crearemos una vpc, 
denominada **fastapi-tdd-vpc**. Esta vpc tendra 2 subredes publicas y 2 subredes privadas (pensando en un futuro
para ofrecer HA. Ademas crearemos sus tablas de rutado, y crearemos un NatGateway (seria mas HA 2) con una IP eleastica
y un Internet Gateway.  Tambien creamos una ip elastica para asignarle al NAT.Estos recursos , para que sea algo mas flexibles , tienen los CIDR(rangos de ips) y los
Availability Zones, definidos variables en **variables.tf**. 


```terraform
# Production VPC
resource "aws_vpc" "fastapi-tdd-vpc" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_support   = true
  enable_dns_hostnames = true
}

# Public subnets
resource "aws_subnet" "public-subnet-1" {
  cidr_block        = var.public_subnet_1_cidr
  vpc_id            = aws_vpc.fastapi-tdd-vpc.id
  availability_zone = var.availability_zones[0]
}
resource "aws_subnet" "public-subnet-2" {
  cidr_block        = var.public_subnet_2_cidr
  vpc_id            = aws_vpc.fastapi-tdd-vpc.id
  availability_zone = var.availability_zones[1]
}

# Private subnets
resource "aws_subnet" "private-subnet-1" {
  cidr_block        = var.private_subnet_1_cidr
  vpc_id            = aws_vpc.fastapi-tdd-vpc.id
  availability_zone = var.availability_zones[0]
}
resource "aws_subnet" "private-subnet-2" {
  cidr_block        = var.private_subnet_2_cidr
  vpc_id            = aws_vpc.fastapi-tdd-vpc.id
  availability_zone = var.availability_zones[1]
}

# Route tables for the subnets
resource "aws_route_table" "public-route-table" {
  vpc_id = aws_vpc.fastapi-tdd-vpc.id
}
resource "aws_route_table" "private-route-table" {
  vpc_id = aws_vpc.fastapi-tdd-vpc.id
}

# Associate the newly created route tables to the subnets
resource "aws_route_table_association" "public-route-1-association" {
  route_table_id = aws_route_table.public-route-table.id
  subnet_id      = aws_subnet.public-subnet-1.id
}
resource "aws_route_table_association" "public-route-2-association" {
  route_table_id = aws_route_table.public-route-table.id
  subnet_id      = aws_subnet.public-subnet-2.id
}
resource "aws_route_table_association" "private-route-1-association" {
  route_table_id = aws_route_table.private-route-table.id
  subnet_id      = aws_subnet.private-subnet-1.id
}
resource "aws_route_table_association" "private-route-2-association" {
  route_table_id = aws_route_table.private-route-table.id
  subnet_id      = aws_subnet.private-subnet-2.id
}

# Elastic IP
resource "aws_eip" "elastic-ip-for-nat-gw" {
  vpc                       = true
  associate_with_private_ip = "10.0.0.5"
  depends_on                = [aws_internet_gateway.production-igw]
}

# NAT gateway
resource "aws_nat_gateway" "nat-gw" {
  allocation_id = aws_eip.elastic-ip-for-nat-gw.id
  subnet_id     = aws_subnet.public-subnet-1.id
  depends_on    = [aws_eip.elastic-ip-for-nat-gw]
}
resource "aws_route" "nat-gw-route" {
  route_table_id         = aws_route_table.private-route-table.id
  nat_gateway_id         = aws_nat_gateway.nat-gw.id
  destination_cidr_block = "0.0.0.0/0"
}

# Internet Gateway for the public subnet
resource "aws_internet_gateway" "production-igw" {
  vpc_id = aws_vpc.fastapi-tdd-vpc.id
}

# Route the public subnet traffic through the Internet Gateway
resource "aws_route" "public-internet-igw-route" {
  route_table_id         = aws_route_table.public-route-table.id
  gateway_id             = aws_internet_gateway.production-igw.id
  destination_cidr_block = "0.0.0.0/0"
}
``` 

Y a continuacion definimos las variables que nos permitiran configurar las ips de las subredes,
junto con las zonas de disponibiliad.

```terraform
# variables networking 

variable "public_subnet_1_cidr" {
  description = "CIDR Block for Public Subnet 1"
  default     = "10.0.1.0/24"
}
variable "public_subnet_2_cidr" {
  description = "CIDR Block for Public Subnet 2"
  default     = "10.0.2.0/24"
}
variable "private_subnet_1_cidr" {
  description = "CIDR Block for Private Subnet 1"
  default     = "10.0.3.0/24"
}
variable "private_subnet_2_cidr" {
  description = "CIDR Block for Private Subnet 2"
  default     = "10.0.4.0/24"
}
variable "availability_zones" {
  description = "Availability zones"
  type        = list(string)
  default     = ["us-east-1a", "us-east-1b"]
}
```
Tambien tenemos que definir los security groups para nuestras maquinas y servicios. Para facilitar el mantenimiento, 
creamos los security groups en un fichero llamado **security_groups.tf**. De momento definiremos los security groups
para el load balancer  que utilizaremos junto con ECS, abriendo el 8000 y el 80 , y despues otro security group para las tareas de ECS a las que 
redirijira el trafico el ELB. Este segundo security group solo permitira trafico que haya pasado el security gourp de
ELB, es decir trafico que acepta el ELB;-)

```terraform
# ALB Security Group (Traffic Internet -> ALB)
resource "aws_security_group" "load-balancer" {
  name        = "load_balancer_security_group"
  description = "Controls access to the ALB"
  vpc_id      = aws_vpc.fastapi-tdd-vpc.id

  ingress {
    from_port   = 8000
    to_port     = 8000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# ECS Security group (traffic ALB -> ECS)
resource "aws_security_group" "ecs" {
  name        = "ecs_security_group"
  description = "Allows inbound access from the ALB only"
  vpc_id      = aws_vpc.fastapi-tdd-vpc.id

  ingress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    security_groups = [aws_security_group.load-balancer.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
```

Por ultimo definimos los outputs que queremos ver tras la creación de recursos. Para esto crearemos un fichero 
llamado **outputs.tf**.Visualizaremos los id de las VPC, subredes y security group. 
Despues estos valores los utilizaremos para desplegar el servicio Dockerizado en AWS ECS correctamente.



```terraform
output "vpc_id" {
  value = aws_vpc.fastapi-tdd-vpc.id
}

output "ecs_security_group_id" {
  value = aws_security_group.ecs.id
}

output "load_balancer_security_group_id" {
  value = aws_security_group.load-balancer.id
}

output "public_subnet_1_id" {
  value = aws_subnet.public-subnet-1.id
}

output "public_subnet_2_id" {
  value = aws_subnet.public-subnet-2.id
}

output "private_subnet_1_id" {
  value = aws_subnet.private-subnet-1.id
}

output "private_subnet_2_id" {
  value = aws_subnet.private-subnet-2.id
}

```

Y ahora creamos la VPC;-)No olvideis actualizar las credenciales de AWS Academy;-)

```terraform
terraform init (para instalar el plugin AWS de Terraform)
terraform validate
terraform plan
  Plan: 18 to add, 0 to change, 0 to destroy.
terraform apply
    Apply complete! Resources: 18 added, 0 changed, 0 destroyed.
    
    Outputs:
    
    ecs_security_group_id = "sg-08f46158d483a5741"
    load_balancer_security_group_id = "sg-0e57e4d0e2bb36d6f"
    private_subnet_1_id = "subnet-0f51d79446510d2c5"
    private_subnet_2_id = "subnet-069a481bb3ec979ae"
    public_subnet_1_id = "subnet-030b1d18d55d91b85"
    public_subnet_2_id = "subnet-097bd9e3210f66c08"
    vpc_id = "vpc-015107dcfb8cb9162"
```

Como todavia no tenemos la aplicacion en esta VPC ni la base de datos RDS podemos destruir todo lo creado para no gastar 
dinero (el NAT GATEWAY) , y ya volveremos a crear caundo nos haga falta;-)

```terraform
terraform destroy
```

13.2- Infrastructura aplicación : ECS, RDS, ELB y SECRETS

Ahora vamos a crear la infraestructura para nuestro servicio. Para ello crearemos una carpeta denominada **service**.
Y dentro de esta pensando que en un futuro  tendremos entorno de produccion y development(staging), 
de momento creamos  la carpeta de producción y development(staging). Ahora comenzamos con la carpeta **development**.
En esta carpeta para comenzar empezamos creando **main.tf** y **variables.tf**.En estos de momento solo indicamos el 
backend y la region;-)

Como en un futuro queremos gestionar varios entornos, que serán muy parecidos , utilizaremos modulos de terraform
para reutilizar concpetos en produccion y staging, por ejemplo. Los diferentes entornos seran iguales con la unica 
diferencia en los nombres de los elemntos donde añadiremos un prefijo/sufijo en base al entorno **${var.environment_name}-**.

Los conceptos/recursos a reutilizar son los siguientes:

- ecr.tf
- ecs.tf
- ecs_service.tf
- iam.tf
- elb.tf
- logs.tf
- outputs.tf
- variables.tf

Primero empezamos crendo el ELB jutno con su Traget group y listeners.Este ELB utilizaremos para rutar las 
entradas al contenedor que desplegaremos en ECS.Definimos el Load Balancer, su target group y el listener.
Aqui la caracteristica principal es que hemos puesto el puerto 8000. Ponemos el ELB com External facing 
Y asociamos al ELB el security group y los subreds publicas creadas anteriormente. Tambien inidcamos el VPC via variable.
Recordad en escribir bien el endpoint de Health-check que tengais , en este caso hemos utilizados **/health-check**.
Y ponmemos escuchando el target group en el puerto 8000. y el listener del ELB tambien.

```terraform
resource "aws_lb" "load_balancer" {
  name               = "${var.environment_name}-alb"
  load_balancer_type = "application"
  internal           = false
  security_groups    = [var.load_balancer_security_group_id]
  subnets            = [var.public_subnet_1_id, var.public_subnet_2_id]
}

# Target group client
resource "aws_alb_target_group" "default-target-group" {
  name     = "${var.environment_name}-client-tg"
  port     = 8000
  protocol = "HTTP"
  vpc_id   = var.vpc_id
  depends_on = [aws_lb.load_balancer]

  health_check {
    path                = "/health-check/"
    port                = "traffic-port"
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 2
    interval            = 5
    matcher             = "200"
  }
}

# Target group users
resource "aws_alb_target_group" "users-target-group" {
  name     = "${var.environment_name}-users-tg"
  port     = var.container_port
  protocol = "HTTP"
  vpc_id   = var.vpc_id

  health_check {
    path                = "/health-check/"
    port                = "traffic-port"
    healthy_threshold   = 5
    unhealthy_threshold = 2
    timeout             = 2
    interval            = 5
    matcher             = "200"
  }
}

# Listener (redirects traffic from the load balancer to the target group)
resource "aws_alb_listener" "ecs-alb-http-listener" {
  load_balancer_arn = aws_lb.load_balancer.id
  port              = "8000"
  protocol          = "HTTP"
  depends_on        = [aws_alb_target_group.default-target-group]

  default_action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.default-target-group.arn
  }
}
```


Una vez definido el ELB pasamo a definir el repositirio de contenedores ECR en el fichero **ecr.tf**.
Aqui crearemos el repositio en base al valor de la variable **environment_name**. Nosotros le hemos dado 
el valor **environment_name = "fastapi-tdd-dev"**. Por lo tanto este terraform creara el ECR  ese nombre.
Le ponemos al ECR uan politica de que mantenga solo las 10 ultimas imagenes. 


```terraform
resource "aws_ecr_repository" "fastapi-tdd" {
  name                 = var.environment_name
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }
}

resource "aws_ecr_lifecycle_policy" "main" {
  repository = aws_ecr_repository.fastapi-tdd.name

  policy = jsonencode({
   rules = [{
     rulePriority = 1
     description  = "keep last 10 images"
     action       = {
       type = "expire"
     }
     selection     = {
       tagStatus   = "any"
       countType   = "imageCountMoreThan"
       countNumber = 10
     }
   }]
  })
}
```
![TF_ECR](doc/images/terraform_ecr1.png)

Ahora pasamos a crear el cluster ECS y el servicio. Esto podriamos ponerlo todo en un fichero.Empezemos creando 
el cluster sobre el que desplegaremos el servicio ECS.

```terraform
resource "aws_ecs_cluster" "fastapi-tdd-cluster" {
  name = var.environment_name
}
```

Y ahora vamos con el servicio ECS. Aqui definiremos la plantilla del Task ECS y el servicio ECS que desplegara el TASK.
En la plantilla Task definiremos el contenedor que queremos desplegar, con sus variables de entorno y  los roles 
que le daremos para su ejecución, y asi poder usar en el codigo servicios AWS via boto3 , por ejemplo los
secrets de AWS. Tambien indicaremos la red en la que queremos desplegar el servicio basado en la plantilla de tarea.
Indicaremos que queremos utilizar un ELB enfrente del servicio, y que a las instancias de conteendores no les 
daremos ips publicas. Todo esto lo definimos en el fichero **ecs_service.tf**. En el contenedor tambien hemos
especificado que los los iran a Cloudwatch.Para ello luego configuraremos tambien cloudwatch.

```terraform
resource "aws_ecs_task_definition" "app" {
  family                = "${var.environment_name}-app"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = 256
  memory                   = 512
  execution_role_arn       = var.execution_role_arn
  task_role_arn            = var.task_role_arn
  container_definitions = <<EOF
  [
        {
          "name": "fastapi-tdd-app",
          "image": "${aws_ecr_repository.fastapi-tdd.repository_url}:latest",
          "cpu": 256,
          "memory": 512,
          "essential": true,
          "environment": [
                {
                    "name": "APP_ENVIRONMENT",
                    "value": "${var.app_environment}"
                }
            ],
          "portMappings": [
            {
              "containerPort": ${var.container_port}
            }
          ],
          "logConfiguration": {
            "logDriver": "awslogs",
            "options": {
              "awslogs-create-group": "true",
              "awslogs-group": "${aws_cloudwatch_log_group.fastapi-tdd-log-group.name}",
              "awslogs-region": "${var.region}",
              "awslogs-stream-prefix": "${aws_cloudwatch_log_stream.fastapi-tdd-log-stream.name}"
            }
          }
        }
    ]
    EOF
}


resource "aws_ecs_service" "fastapi-tdd-service"{
  name                               = var.environment_name
  cluster                            = aws_ecs_cluster.fastapi-tdd-cluster.name
  task_definition                    = aws_ecs_task_definition.app.family
  desired_count                      = var.app_count
  deployment_minimum_healthy_percent = 50
  deployment_maximum_percent         = 200
  launch_type                        = "FARGATE"
  scheduling_strategy                = "REPLICA"


   network_configuration {
     security_groups  = [var.ecs_security_group_id]
     subnets          = [var.private_subnet_1_id, var.private_subnet_2_id]
     assign_public_ip = false
   }

    load_balancer {
      target_group_arn = aws_alb_target_group.default-target-group.arn
      container_name   = "fastapi-tdd-app"
      container_port   = var.container_port
    }
    depends_on = [aws_alb_listener.ecs-alb-http-listener]

    lifecycle {
      ignore_changes = [task_definition]
    }
}
``` 
Ahora creamos un grupo y un stream cloudwatch donde enviaremos los logs.


```terraform
resource "aws_cloudwatch_log_group" "fastapi-tdd-log-group" {
  name              = "/ecs/${var.environment_name}"
  retention_in_days = var.log_retention_in_days
}

resource "aws_cloudwatch_log_stream" "fastapi-tdd-log-stream" {
  name           = "${var.environment_name}-app-log-stream"
  log_group_name = aws_cloudwatch_log_group.fastapi-tdd-log-group.name
}  
```

Ahora creemos la base de datos y con el las credenciales de acceso. Mediante el endpoitn y las credenciales
crearemos como secreto el URI de la base de datos, el cual lo leermos desde el modulo **config.py** de nuestro 
código via boto3.

Primero veamos como creamos la base de datos.La base de datos la ubicamos en las subredes privadas.
Crearemos un security group para la base de datos onde solo abrimos el puerto 5432.
Las credenciales las crearemos de forma random. Al crear la base ded datos le pasamos el password 
random creado, el nombre del usuario sera **app**, al igual que el nombre de la base de datos.
Utilizaremos instancia minimas **db.t3.micro**. Debido a AWS Academy le indicamos que no queremos multiAZ.
Y al final creamos el secreto con toda la información. El secreto se llama **db-connection-string-${var.app_environment}**
que en nuestro caso sera **db-connection-string-development**.Siendo su valor:

"postgresql+asyncpg://${aws_db_instance.fastapi-tdd.username}:${random_password.db_password.result}@${aws_db_instance.fastapi-tdd.endpoint}/${aws_db_instance.fastapi-tdd.db_name}"

Es importante decir , que al secreto  le hemos puestos una politica parecida a la de pr defecto,
es decir , que al bprrar el secretoi esta no termina de eleminarse hasta que finalzia su ventada (7 dias). Esto se suele poner por
si tenemos que recuperar el secreto. Denido a esto si hacemos **terraform destroy** y luego **apply**, 
os dira que no puede crear el secreto porque ya existe. Para borrar hay que utilizar una llamada concreta 
al API de aws. Lo teneis a continuación.

```bash
aws secretsmanager delete-secret --secret-id db-connection-string-development  --force-delete-without-recovery --region us-east-1
terraform refresh
```
Y aqui teneis el terraform que crea el RDS junto con su secreto.

```terraform
resource "aws_db_subnet_group" "main" {
  name       = var.environment_name
  subnet_ids = [var.private_subnet_1_id, var.private_subnet_2_id]
}

resource "random_password" "db_password" {
  length  = 35
  special = true
  override_special = "!#$%&*()-_=+[]{}<>:?"
}

resource "aws_security_group" "rds" {
  name        = var.environment_name
  description = "Allows inbound access from ECS only"
  vpc_id      = var.vpc_id

  ingress {
    protocol        = "tcp"
    from_port       = "5432"
    to_port         = "5432"
    security_groups = [var.ecs_security_group_id]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_db_instance" "fastapi-tdd" {
  identifier              = var.environment_name
  db_name                    = "app"
  username                = "app"
  password                = random_password.db_password.result
  port                    = "5432"
  engine                  = "postgres"
  # engine_version          = "13.7"
  instance_class          = "db.t3.micro"
  allocated_storage       = "20"
  storage_encrypted       = false
  vpc_security_group_ids  = [aws_security_group.rds.id]
  db_subnet_group_name    = aws_db_subnet_group.main.name
  multi_az                = false
  storage_type            = "gp2"
  publicly_accessible     = false
  backup_retention_period = 7
  skip_final_snapshot     = true
}

resource "aws_secretsmanager_secret" "db_connection_string" {
  name                    = "db-connection-string-${var.app_environment}"
  description             = "Database connection string"
  recovery_window_in_days = 14
}

resource "aws_secretsmanager_secret_version" "db_connection_string" {
  secret_id     = aws_secretsmanager_secret.db_connection_string.id
  secret_string = "postgresql+asyncpg://${aws_db_instance.fastapi-tdd.username}:${random_password.db_password.result}@${aws_db_instance.fastapi-tdd.endpoint}/${aws_db_instance.fastapi-tdd.db_name}"
} 
```

Como ahora tanto en producción como en staging utilizamos secretos de AWS para obtrener el connection string ,
modificamos la clase  python que gestiona la  configuración **project/service/config.py**.


```python
class BaseConfig:
    DEBUG = False
    TESTING = False
    APP_ENVIRONMENT = "production"
    _SQLALCHEMY_DATABASE_URL = None


# entorno produccion
class ProductionConfig(BaseConfig):
    _SQLALCHEMY_DATABASE_URL = ""

    @property
    def SQLALCHEMY_DATABASE_URL(self):
        if self._SQLALCHEMY_DATABASE_URL is None:
            self._SQLALCHEMY_DATABASE_URL = boto3.client(
                "secretsmanager"
            ).get_secret_value(SecretId=f"db-connection-string-{self.APP_ENVIRONMENT}")[
                "SecretString"
            ]

        return self._SQLALCHEMY_DATABASE_URL


# cccnmn podria ser uan especie de entorno staging
class DevelopmentConfig(ProductionConfig):
    DEBUG = True
    APP_ENVIRONMENT = "development"
    # SQLALCHEMY_DATABASE_URL = os.environ.get("DATABASE_DEV_URL")
    _SQLALCHEMY_DATABASE_URL = None

    @property
    def SQLALCHEMY_DATABASE_URL(self):
        if self._SQLALCHEMY_DATABASE_URL is None:
            self._SQLALCHEMY_DATABASE_URL = boto3.client(
                "secretsmanager"
            ).get_secret_value(SecretId=f"db-connection-string-{self.APP_ENVIRONMENT}")[
                "SecretString"
            ]

        return self._SQLALCHEMY_DATABASE_URL


# Entorno CI/CD  pipeline (docker)
class TestConfig(BaseConfig):
    TESTING = True
    DEBUG = False
    APP_ENVIRONMENT = "local"  # iguel hobeto pipeline/cI
    SQLALCHEMY_DATABASE_URL = os.environ.get("DATABASE_TEST_URL")
```

Al utilizar boto3 en el código, este necesita saber la region de AWS de trabajo y las credenciales.
Hasta ahora nuestro codigo  fastapi , no utiliza nada de AWS, pero ahora si. Por lo tanto, si ahora
ejecutamos los test con docker-compose en local nos dire que el contenedor no tiene credenciales, ya que
las credenciales no estan en el conetendor estan en el **~/.aws/** del ordenador , y no del contenedor.
Los test ejecutandolos con poetry desde fuera del conteendor , vuestro oredandor, iran bien, 

```bash
poetry run pytest tests/unit
```

pero desde el docker os dara error de credenciales. 

```bash
docker-compose exec web pytest tests/unit
```

Para solucionar esto , en desarrollo local utilziaremos volumens pasandolo tanto el fichero **.aws/credentials*
como **.aws/configure**. Y en el pipeline tendremos que agregarle la region por defecto. El
docker-compose del servicio web , sera: 

```yml
services:
  web:
    build: ./project
    command: uvicorn services.main:app --reload --workers 1 --host 0.0.0.0 --port 8000
    volumes:
      - ./project:/usr/src/app
      - ~/.aws/credentials:/root/.aws/credentials:ro
      - ~/.aws/config:/root/.aws/config:ro
    ports:
      - 8004:8000
    environment:
      - APP_ENVIRONMENT=local_test
      - DATABASE_LOCAL_URL=postgresql+asyncpg://postgres:postgres@web-db:5432/web_dev
      - DATABASE_LOCAL_TEST_URL=postgresql+asyncpg://postgres:postgres@web-db:5432/web_test
    depends_on:
      - web-db
```

Y en el pipeline de gitlab creamos la siguiente variable de entorno.
```bash
AWS_DEFAULT_REGION
```
![TF_CI_AWS_REGION](doc/images/terraform_ci_aws_region.png)

El siguiente paso es genera los outputs.Como outputs del servico daremos el URI del repositorio ECR 
creado, y por otro lado la URL del ELB que nos reenviara al servicio Docker credao en ECS.

```terraform
output "load_balancer_dns" {
  value = aws_lb.load_balancer.dns_name
}

output "ecr_url" {
  value = aws_ecr_repository.fastapi-tdd.repository_url
}
```

La idea del modulo es luego utilizar con diferentes valores , en el fichero variables.tf definimos 
las variables que utilziaremso para crear el servcio en diferentes entornos configuraciones;-)
```terraform
variable "region" {
  description = "The AWS region to create resources in."
}
variable "vpc_id" {
  description = "ID od VPC"
  type = string
}

variable "environment_name" {
  description = "Name of app environment. Must be unique."
  type = string
}

variable "instance_type" {
  description = "Type of EC2 instance"
  type = string
}

variable "ecs_security_group_id" {
  description = "ID of ECS security group"
  type = string
}
variable "load_balancer_security_group_id" {
  description = "ID of ALB security group"
  type = string
}

variable "log_retention_in_days" {
  description = "Log retention in days"
  type = number
}

variable "public_subnet_1_id" {
  description = "Id of first public subnet"
  type = string
}

variable "public_subnet_2_id" {
  description = "Id of second public subnet"
  type = string
}

variable "private_subnet_1_id" {
  description = "Id of first private subnet"
  type = string
}

variable "private_subnet_2_id" {
  description = "Id of second private subnet"
  type = string
}

variable "autoscale_min" {
  description = "Minimum autoscale (number of EC2)"
}
variable "autoscale_max" {
  description = "Maximum autoscale (number of EC2)"
}
variable "autoscale_desired" {
  description = "Desired autoscale (number of EC2)"
}

variable "app_count" {
  description = "Desired number of running apps"
  type = string
}

variable "container_port" {
  description = "App container port"
  type = number
  default = 8000
}

variable "app_environment" {
  description = "Application environment"
  type = string
}

variable  "execution_role_arn"{
  description = "ecs_task_execution_role.arn"
  type = string
}

variable "task_role_arn"{
  description = "ecs_task_role.arn"
  type = string

}

variable "iam_role"{
  description = "ecs-service-role.arn"
  type = string

}

```

Y ahora que hemos creado el modulo creamos el servicio reutilizando este modulo;-)
Los siguientes tf los crearemos bajo la carpeta **infrastructure/services/development**.

Aqui tenemos un main.tf donde le indicamos que coja los nombres de la VPC , subredes del terraform anaterior.
Para esto utilziamos el tipo data remote_state de terraform. Ahi le indicamos donde esta el estado del terrafomr de VPC..

```terraform
data "terraform_remote_state" "vpc" {
  backend = "local"

  config = {
    path = "../../global/vpc/terraform.tfstate"
  }
}

```
Y ahora instanciaremos un  modulo reutilizando el codigo anterior **source = "../../modules/fastapi-tdd**. 
le damos valores a las variables de modulo y adelante;-) Todo esto tambien en el main.tf. En las variables lo qu ecambiara en vuestr caso sera el role concreto 
de vuestro usuario AWS Academy Vocareum del learner lab que esteis utilizando. En este ejemplo es
**arn:aws:iam::662960992834:role/LabRole**.

Es importante decir , que para determinar cual es nuestro entorno hemos utilizado los siguientes valores,

environment_name = "fastapi-tdd-dev"
app_environment = "development"

Tambien seria interesante utilizar estos valores para crear los dominios de app.com y dev.app.com.

```terraform
terraform{
  backend "local"{}
}

data "terraform_remote_state" "vpc" {
  backend = "local"

  config = {
    path = "../../global/vpc/terraform.tfstate"
  }
}

provider "aws" {
  region = var.region
}

module "fastapi-tdd-service" {
  source = "../../modules/fastapi-tdd"


  vpc_id = data.terraform_remote_state.vpc.outputs.vpc_id // "vpc-0e2859b192444dd56"
  ecs_security_group_id = data.terraform_remote_state.vpc.outputs.ecs_security_group_id // "sg-05fc775dfed946b74"
  load_balancer_security_group_id = data.terraform_remote_state.vpc.outputs.load_balancer_security_group_id// "sg-05fc775dfed946b74"
  public_subnet_1_id = data.terraform_remote_state.vpc.outputs.public_subnet_1_id// "subnet-0a413a2243f0cfeb8"
  public_subnet_2_id = data.terraform_remote_state.vpc.outputs.public_subnet_2_id// "subnet-0c542c4d102e094ff"
  private_subnet_1_id = data.terraform_remote_state.vpc.outputs.private_subnet_1_id// "subnet-08033e39d51d783a2"
  private_subnet_2_id = data.terraform_remote_state.vpc.outputs.private_subnet_2_id//"subnet-04456c48b8e908718"

  instance_type = "t3.small"
  log_retention_in_days = 30
  autoscale_min = 1
  autoscale_desired = 1
  autoscale_max = 4
  region = var.region
  app_count = 1
  environment_name = "fastapi-tdd-dev"
  app_environment = "development"

  execution_role_arn="arn:aws:iam::662960992834:role/LabRole"
  task_role_arn="arn:aws:iam::662960992834:role/LabRole"
  iam_role="arn:aws:iam::662960992834:role/LabRole"
}
```
Por ultimo falta indicar la region , esto lo hacemos en variables.tf

```terraform
variable "region" {
  description = "The AWS region to create resources in."
  default     = "us-east-1"
}
```

Y ahora solo queda desplegar la infraestructura y ejecutar el pipeline. Vamos con ello

```terraform
terraform plan --out json
terraform apply json
```
Comrpobamos que se haya creado eluster , servicio , ecr y elb.

![TF_CI_AWS_REGION](doc/images/terraform_ecs_cluster.png)

Y ahora ejecutamso el pipeline y comprobamos que se despliegua una nueva version. POr ejemplo probar a aumentar
la version dl health-check a 0.6. Antes de tagear hacer push y lanzar el pipeline, modificar las
variables de enotrno de pipeline referentes al cluster ECS, y modificar el smoke test para que busque la url adecuada 
del servicio.

![TF_CI_AWS_REGION](doc/images/terraform_ci_var_ecs1.png)
![TF_CI_AWS_REGION](doc/images/terraform_ci_var_ecs2.png)

```bash
#!/bin/bash
#ToDo paremetrizar normbre servicio
target_group_arn=`aws ecs describe-services --cluster fastapi-tdd-dev  --services fastapi-tdd-dev  --region us-east-1 | jq .services[].loadBalancers[].targetGroupArn|  tr -d '"'`
```

Y ahora si, lanzamos el pipeline;-)

```bash
git tag -a v0.13.12 -m 'ENV staging Healtcheck OK 0.6 + e2e + ECS + RDS + secrets + subnet privado Terraform'
git push origin master --tags
```

Y ahora comprobamos que ha ido bien.

![TF_CI_AWS_REGION](doc/images/terraform_ci_pipeline.png)
![TF_CI_AWS_REGION](doc/images/terraform_ci_pipeline2.png)


Y finalmente navegamos al elb y comrpobamos que el servidor funciona correctamente;-)
![TF_CI_AWS_REGION](doc/images/terraform_ecs_working.png)
