output "bastion_dns" {
  value = aws_instance.bastion.public_dns
}

output "load_balancer_dns" {
  value =aws_lb.load_balancer.dns_name
}
