# Bastion EC2 launch configuration
resource "aws_instance" "bastion" {
  ami                         = var.ami
  instance_type               = var.bastion-instance-type
  key_name                    = var.key-name
  security_groups             = [var.apps_security_group_id]
  subnet_id                   = var.public_subnet_1_id 
  associate_public_ip_address = true
  user_data                   = data.template_file.bastion.rendered
  iam_instance_profile        = "LabInstanceProfile"
  lifecycle {
    create_before_destroy = true
  }
  tags = {
    "Name" = "macc-bastion"
  }
  # Copies the ssh key file to home dir
  provisioner "file" {
    source      = "./key/${var.key-name}.pem"
    destination = "/home/ec2-user/${var.key-name}.pem"
    connection {
      type        = "ssh"
      user        = "ec2-user"
      private_key = file("./key/${var.key-name}.pem")
      host        = self.public_ip
    }
  }
  # chmod key 400 on EC2 instance
  provisioner "remote-exec" {
    inline = ["chmod 400 ~/${var.key-name}.pem"]
    connection {
      type        = "ssh"
      user        = "ec2-user"
      private_key = file("./key/${var.key-name}.pem")
      host        = self.public_ip
    }
  }
}

resource "aws_instance" "posts" {
  ami                         = var.ami
  instance_type               = var.bastion-instance-type
  key_name                    = var.key-name
  security_groups             = [var.apps_security_group_id]
  subnet_id                   = var.private_subnet_1_id 
  associate_public_ip_address = false
  user_data                   = data.template_file.posts.rendered
  iam_instance_profile        = "LabInstanceProfile"
  lifecycle {
    create_before_destroy = true
  }
  tags = {
    "Name" = "macc-posts"
  }

  # depends_on = [aws_db_instance.fastapi-tdd]
}
