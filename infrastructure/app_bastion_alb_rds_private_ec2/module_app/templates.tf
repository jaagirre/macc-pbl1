# User data templates
data "template_file" "bastion" {
  template = file("${path.module}/user_data/bastion.sh")

  vars = {
    bastion-hostname      = "bastion"
    # service-port          = 443
    # kibana-host           = aws_instance.microservices-observability.private_ip 
    aws-region            = var.region
  }
}


# User data templates
data "template_file" "posts" {
  template = file("${path.module}/user_data/posts.sh")

  vars = {
    bastion-hostname      = "posts"
    aws-region            = var.region
    app-environment = var.app_environment     
    rds_secrets_value = aws_secretsmanager_secret_version.db_connection_string.secret_string
  }
}

