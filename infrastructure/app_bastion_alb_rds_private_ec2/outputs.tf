output "bastion" {
  description="Bastion public DNS"
  value = module.service.bastion_dns
}

output "load_balancer_dns" {
   value = module.service.load_balancer_dns
}

