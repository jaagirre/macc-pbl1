#!/bin/bash -ex
exec > >(tee /var/log/user-data.log | logger -t user-data) 2>&1
sysctl kernel.hostname="${bastion-hostname}"
yum -y update
service docker start
usermod -a -G docker ec2-user



export PUBLIC_IP=$(curl http://169.254.169.254/latest/meta-data/public-ipv4)
export AWS_DEFAULT_REGION=${aws-region}

# Get needed resources from bucket



# Create self-signed certificate and move it to needed directory
sudo -u ec2-user mkdir -p "/home/ec2-user/${bastion-hostname}/haproxy/certs"
sudo -u ec2-user openssl req -newkey rsa:4096 -x509 -sha256 -days 3650 -nodes \
  -out "/home/ec2-user/${bastion-hostname}.crt" \
  -keyout "/home/ec2-user/${bastion-hostname}.pem" \
  -subj "/C=ES/ST=Gipuzkoa/L=Arrasate-Mondragón/O=MACC/OU=Curso 1/CN=$${PUBLIC_IP}"
sudo -u ec2-user touch "/home/ec2-user/${bastion-hostname}/haproxy/certs/haproxy_ssl.pem"
cat "/home/ec2-user/bastion.pem" "/home/ec2-user/bastion.crt" >"/home/ec2-user/${bastion-hostname}/haproxy/certs/haproxy_ssl.pem"

# Create .env for docker-compose
sudo -u ec2-user touch "/home/ec2-user/${bastion-hostname}/.env"

# Get needed secrets

su - ec2-user -c "docker-compose -f /home/ec2-user/macc-pbl1/docker-compose.yml up"

echo "#!/bin/bash" | sudo tee /etc/init.d/my_startup_script.sh
echo "su - ec2-user -c 'docker-compose -f /home/ec2-user/macc-pbl1/docker-compose.yml up'" | sudo tee -a /etc/init.d/my_startup_script.sh
sudo chmod +x /etc/init.d/my_startup_script.sh
sudo chkconfig --add my_startup_script.sh