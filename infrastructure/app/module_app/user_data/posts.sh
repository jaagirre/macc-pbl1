#!/bin/bash -ex
exec > >(tee /var/log/user-data.log | logger -t user-data) 2>&1
sysctl kernel.hostname="${bastion-hostname}"
yum -y update
service docker start
usermod -a -G docker ec2-user



export PUBLIC_IP=$(curl http://169.254.169.254/latest/meta-data/local-ipv4)
export AWS_DEFAULT_REGION=${aws-region}
export APP_ENVIRONMENT=${app-environment}

# Get needed resources from bucket



# Create self-signed certificate and move it to needed directory
sudo -u ec2-user mkdir -p "/home/ec2-user/${bastion-hostname}/haproxy/certs"
sudo -u ec2-user openssl req -newkey rsa:4096 -x509 -sha256 -days 3650 -nodes \
  -out "/home/ec2-user/${bastion-hostname}.crt" \
  -keyout "/home/ec2-user/${bastion-hostname}.pem" \
  -subj "/C=ES/ST=Gipuzkoa/L=Arrasate-Mondragón/O=MACC/OU=Curso 1/CN=$${PUBLIC_IP}"
sudo -u ec2-user touch "/home/ec2-user/${bastion-hostname}/haproxy/certs/haproxy_ssl.pem"
cat "/home/ec2-user/bastion.pem" "/home/ec2-user/bastion.crt" >"/home/ec2-user/${bastion-hostname}/haproxy/certs/haproxy_ssl.pem"

# Create .env for docker-compose
sudo -u ec2-user touch "/home/ec2-user/${bastion-hostname}/.env"

# Get needed secrets

#
# aws-region            = var.region
#    app-environment 
su - ec2-user -c 'export AWS_DEFAULT_REGION=${aws-region}'
su - ec2-user -c export APP_ENVIRONMENT=${app-environment}


su - ec2-user -c 'docker build -f /home/ec2-user/macc-pbl1/src/Dockerfile.prod  /home/ec2-user/macc-pbl1/src -t posts'
su - ec2-user -c 'docker run -d -p 8000:8000 --name posts -e APP_ENVIRONMENT=development -e AWS_DEFAULT_REGION=us-east-1 posts' 

