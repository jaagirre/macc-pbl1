resource "aws_lb" "load_balancer" {
  name               = "${var.app_environment}-alb"
  load_balancer_type = "application"
  internal           = false
  security_groups    = [var.load_balancer_security_group_id]
  subnets            = [var.public_subnet_1_id, var.public_subnet_2_id]
}

# Target group client
resource "aws_alb_target_group" "default-target-group" {
  name     = "${var.app_environment}-client-tg"
  port     = 8000
  protocol = "HTTP"
  vpc_id   = var.vpc_id
  target_type = "ip"
  depends_on = [aws_lb.load_balancer]


  health_check {
    path                = "/health-check/"
    port                = "traffic-port"
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 2
    interval            = 5
    matcher             = "200"
  }

}



# Attach instance to target group
resource "aws_alb_target_group_attachment" "default-target-group-attachment" {
  target_group_arn = aws_alb_target_group.default-target-group.arn
  target_id        = aws_instance.posts.private_ip
  port             = 8000
}


# Listener (redirects traffic from the load balancer to the target group)
resource "aws_alb_listener" "ecs-alb-http-listener" {
  load_balancer_arn = aws_lb.load_balancer.arn
  port              = "8000"
  protocol          = "HTTP"
  depends_on        = [aws_alb_target_group.default-target-group]

  default_action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.default-target-group.arn
  }
}