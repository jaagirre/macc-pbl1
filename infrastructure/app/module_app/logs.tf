resource "aws_cloudwatch_log_group" "pbl1-log-group" {
  name              = "/ecs/${var.app_environment}"
  retention_in_days = var.log_retention_in_days
}

resource "aws_cloudwatch_log_stream" "pbl1-log-stream" {
  name           = "${var.app_environment}-app-log-stream"
  log_group_name = aws_cloudwatch_log_group.pbl1-log-group.name
}  