
variable "region" {
  description = "The AWS region to create resources in."
  default     = "us-east-1"
}


variable "vpc_id" {
  description = "ID od VPC"
  type = string
}


variable "app_environment" {
  description = "Application environment"
  type = string
}


variable "bastion-instance-type" {
  type    = string
  default = "t3.medium"
}

# SSH key name
variable "key-name" {
  type    = string
  default = "macc-pi-2025"
}

variable "ami" {
  type    = string
  default = "ami-0a58dd0832e5e8ecc"
}

variable "apps_security_group_id"{}

variable "load_balancer_security_group_id"{}

variable "log_retention_in_days" {
  description = "Log retention in days"
  type = number
}

variable "public_subnet_1_id" {
  description = "Id of first public subnet"
  type = string
}

variable "public_subnet_2_id" {
  description = "Id of second public subnet"
  type = string
}

variable "private_subnet_1_id" {
  description = "Id of first private subnet"
  type = string
}

variable "private_subnet_2_id" {
  description = "Id of second private subnet"
  type = string
}


