resource "aws_ecs_task_definition" "app" {
  family                = "${var.environment_name}-app"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = 256
  memory                   = 512
  execution_role_arn       = var.execution_role_arn
  task_role_arn            = var.task_role_arn
  container_definitions = <<EOF
  [
        {
          "name": "fastapi-tdd-app",
          "image": "${aws_ecr_repository.fastapi-tdd.repository_url}:latest",
          "cpu": 256,
          "memory": 512,
          "essential": true,
          "environment": [
                {
                    "name": "APP_ENVIRONMENT",
                    "value": "${var.app_environment}"
                }
            ],
          "portMappings": [
            {
              "containerPort": ${var.container_port}
            }
          ],
          "logConfiguration": {
            "logDriver": "awslogs",
            "options": {
              "awslogs-create-group": "true",
              "awslogs-group": "${aws_cloudwatch_log_group.fastapi-tdd-log-group.name}",
              "awslogs-region": "${var.region}",
              "awslogs-stream-prefix": "${aws_cloudwatch_log_stream.fastapi-tdd-log-stream.name}"
            }
          }
        }
    ]
    EOF
}


resource "aws_ecs_service" "fastapi-tdd-service"{
  name                               = var.environment_name
  cluster                            = aws_ecs_cluster.fastapi-tdd-cluster.name
  task_definition                    = aws_ecs_task_definition.app.family
  desired_count                      = var.app_count
  deployment_minimum_healthy_percent = 50
  deployment_maximum_percent         = 200
  launch_type                        = "FARGATE"
  scheduling_strategy                = "REPLICA"


   network_configuration {
     security_groups  = [var.ecs_security_group_id]
     subnets          = [var.private_subnet_1_id, var.private_subnet_2_id]
     assign_public_ip = false
   }

    load_balancer {
      target_group_arn = aws_alb_target_group.default-target-group.arn
      container_name   = "fastapi-tdd-app"
      container_port   = var.container_port
    }
    depends_on = [aws_alb_listener.ecs-alb-http-listener]

    lifecycle {
      ignore_changes = [task_definition]
    }
}


