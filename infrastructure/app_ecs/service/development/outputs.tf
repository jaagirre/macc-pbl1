output "load_balancer_dns" {
  value = module.fastapi-tdd-service.load_balancer_dns
}

output "ecr_url" {
  value = module.fastapi-tdd-service.ecr_url
}