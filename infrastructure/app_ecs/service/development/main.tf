terraform{
  backend "local"{}
}

data "terraform_remote_state" "vpc" {
  backend = "local"

  config = {
    path = "../../global/vpc/terraform.tfstate"
  }
}

provider "aws" {
  region = var.region
}

module "fastapi-tdd-service" {
  source = "../../modules/fastapi-tdd"


  vpc_id = data.terraform_remote_state.vpc.outputs.vpc_id // "vpc-0e2859b192444dd56"
  ecs_security_group_id = data.terraform_remote_state.vpc.outputs.ecs_security_group_id // "sg-05fc775dfed946b74"
  load_balancer_security_group_id = data.terraform_remote_state.vpc.outputs.load_balancer_security_group_id// "sg-05fc775dfed946b74"
  public_subnet_1_id = data.terraform_remote_state.vpc.outputs.public_subnet_1_id// "subnet-0a413a2243f0cfeb8"
  public_subnet_2_id = data.terraform_remote_state.vpc.outputs.public_subnet_2_id// "subnet-0c542c4d102e094ff"
  private_subnet_1_id = data.terraform_remote_state.vpc.outputs.private_subnet_1_id// "subnet-08033e39d51d783a2"
  private_subnet_2_id = data.terraform_remote_state.vpc.outputs.private_subnet_2_id//"subnet-04456c48b8e908718"

  instance_type = "t3.small"
  log_retention_in_days = 30
  autoscale_min = 1
  autoscale_desired = 1
  autoscale_max = 4
  region = var.region
  app_count = 1
  environment_name = "fastapi-tdd-dev"
  app_environment = "development"

  execution_role_arn="arn:aws:iam::877312739380:role/LabRole"
  task_role_arn="arn:aws:iam::877312739380:role/LabRole"
  iam_role="arn:aws:iam::877312739380:role/LabRole"
}