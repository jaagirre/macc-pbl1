output "bastion" {
  description="Bastion public DNS"
  value = module.service.bastion_dns
}