terraform{
  backend "local"{}
}

data "terraform_remote_state" "vpc" {
  backend = "local"

  config = {
    path = "../global/terraform.tfstate"
  }
}

provider "aws" {
  region = var.region
}


module "service" {
  source = "./module_app"


  vpc_id = data.terraform_remote_state.vpc.outputs.vpc_id // "vpc-0e2859b192444dd56"
  apps_security_group_id = data.terraform_remote_state.vpc.outputs.bastion_security_group_id // "sg-05fc775dfed946b74"
  public_subnet_1_id = data.terraform_remote_state.vpc.outputs.public_subnet_1_id// "subnet-0a413a2243f0cfeb8"
  public_subnet_2_id = data.terraform_remote_state.vpc.outputs.public_subnet_2_id// "subnet-0c542c4d102e094ff"
  private_subnet_1_id = data.terraform_remote_state.vpc.outputs.private_subnet_1_id// "subnet-08033e39d51d783a2"
  private_subnet_2_id = data.terraform_remote_state.vpc.outputs.private_subnet_2_id//"subnet-04456c48b8e908718"

  bastion-instance-type = "t3.medium"
  log_retention_in_days = 30
  region = var.region
  app_environment = "macc-development"
  ami="ami-03e620046976b400c"  
}