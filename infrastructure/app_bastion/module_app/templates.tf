# User data templates
data "template_file" "bastion" {
  template = file("${path.module}/user_data/bastion.sh")

  vars = {
    bastion-hostname      = "bastion"
    # service-port          = 443
    # kibana-host           = aws_instance.microservices-observability.private_ip 
    aws-region            = var.region
  }
}
