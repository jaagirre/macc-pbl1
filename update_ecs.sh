# export AWS_ACCESS_KEY_ID=ASIA4YQ7KBA2PWU3AJ2C
# export AWS_SECRET_ACCESS_KEY=/U7Wvz7icHfer9oBTvYRoHBo
# export AWS_SESSION_TOKEN=FwoGZXIvYXdzEDoaDNdi2yBpXlSe
export CI_AWS_ECS_CLUSTER=fastapi-tdd-dev
export CI_AWS_ECS_SERVICE=fastapi-tdd-dev
export CI_AWS_ECS_TASK_DEFINITION=fastapi-tdd-dev-app
export REPOSITORY_URL=877312739380.dkr.ecr.us-east-1.amazonaws.com/fastapi-tdd-dev
export IMAGE_TAG=registry.gitlab.com/macc_ci_cd/ci_cd/labtdd-fastapi-example:latest

aws ec2 describe-instances --region us-east-1
# docker login registry.gitlab.com
# docker pull   registry.gitlab.com/macc_ci_cd/ci_cd/labtdd-fastapi-example:latest
docker tag  registry.gitlab.com/macc_ci_cd/ci_cd/labtdd-fastapi-example:latest $REPOSITORY_URL:froga
$(aws ecr get-login --no-include-email --region us-east-1)
docker push  $REPOSITORY_URL:froga
echo `aws ecs describe-task-definition --task-definition  $CI_AWS_ECS_TASK_DEFINITION --region us-east-1` > input.json
echo $(cat input.json | jq '.taskDefinition.containerDefinitions[].image="'$REPOSITORY_URL':'froga'"') >  input.json
echo $(cat input.json | jq '.taskDefinition') > input.json
echo $(cat input.json | jq 'del(.taskDefinitionArn) | del(.revision) | del(.status) | del(.requiresAttributes) | del(.compatibilities) | del(.registeredAt)  | del(.registeredBy)')  > input.json
aws ecs register-task-definition --cli-input-json file://input.json --region us-east-1
revision=$(aws ecs describe-task-definition --task-definition $CI_AWS_ECS_TASK_DEFINITION --region us-east-1 | egrep "revision" | tr "/" " " | awk '{print $2}' | sed 's/"$//' | cut -d "," -f 1)
echo $revision
aws ecs update-service --cluster $CI_AWS_ECS_CLUSTER --service $CI_AWS_ECS_SERVICE  --task-definition $CI_AWS_ECS_TASK_DEFINITION:$revision --region us-east-1
echo "Done Revision ===> ${revision}"
