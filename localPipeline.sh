docker-compose exec web pytest tests -p no:warnings --ignore=tests/e2e
docker-compose exec web flake8
docker-compose exec web black . --check
docker-compose exec web /bin/sh -c "isort ./*/*.py --check-only --profile black" q
